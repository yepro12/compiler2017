package com.yepro12.compiler2017.Front;

import com.yepro12.compiler2017.AST.*;
import com.yepro12.compiler2017.IR.Block;
import com.yepro12.compiler2017.Table.*;
import com.yepro12.compiler2017.IR.*;
import com.yepro12.compiler2017.Front.*;
import java.util.*;

import static com.yepro12.compiler2017.Back.X86RegisterSet.*;
import static com.yepro12.compiler2017.Back.X86RegisterSet.R8;
import static com.yepro12.compiler2017.Back.X86RegisterSet.R9;

/**
 * Created by yepro12 on 2017/5/21.
 */
public class IRgenerator implements ASTVisitor {

    public com.yepro12.compiler2017.IR.Block currrentBlock;
    public com.yepro12.compiler2017.IR.Block goContinue;
    public com.yepro12.compiler2017.IR.Block goBreak;
    public Function curFunction;
    public VirtualRegister pointer;
    public Block maininit;
    public ClassType nowClass = null;
    public boolean getAddress = false;
    public ClassType currentClass = null;
    public boolean isArg = false;
    public IRRoot root = new IRRoot();
    public GlobalTable table;
    public Program tmpProgram = null;
    public IRgenerator(GlobalTable table) {
        this.table = table;
    }
    public boolean isLogicalExpression(Expr node) {
        if (node instanceof Binary) {
            Binary.bop op = ((Binary) node).op;
            return op == Binary.bop.Land || op == Binary.bop.Lor;
        } else if (node instanceof Unary) {
            return ((Unary) node).op == Unary.uop.Lnot;
        }
        return false;
    }

    public boolean needMemoryAccess(Expr node) {
        return  node instanceof MemberAccess || node instanceof ArrayAccess ||
                (node instanceof Identifier && ((Identifier) node).classSub != null);
    }

    @Override
    public void visit(Program node) {
        tmpProgram = node;
        System.err.println("begin program");
        for(Declare dec: node.declare)if(dec instanceof ClassDeclare){
            ClassDeclare cd = (ClassDeclare)dec;
            System.err.println(cd.name);
            for(FunctionDeclare mbfc: cd.memberFunc) {
                System.err.println(mbfc.name);
                root.functions.put(mbfc.name, new Function(mbfc.functype));
            }
        }
        node.declare.stream().filter(x->x instanceof FunctionDeclare).forEach(x->root.functions.put
                (((FunctionDeclare)x).name, new Function(((FunctionDeclare)x).functype)));

        for(Declare dc: node.declare)if(dc instanceof ClassDeclare){
            for(FunctionDeclare mbfc: ((ClassDeclare)dc).memberFunc)mbfc.accept(this);
        }
        node.declare.forEach(x -> x.accept(this));
        for(Function nowfunc : root.functions.values()){
            //if(nowfunc.paralist.size() > 0)nowfunc.paralist.get(0).forcedPhysicalRegister = RDI;
            //if(nowfunc.paralist.size() > 1)nowfunc.paralist.get(1).forcedPhysicalRegister = RSI;
            //if(nowfunc.paralist.size() > 2)nowfunc.paralist.get(2).forcedPhysicalRegister = RDX;
            //if(nowfunc.paralist.size() > 3)nowfunc.paralist.get(3).forcedPhysicalRegister = RCX;
            //if(nowfunc.paralist.size() > 4)nowfunc.paralist.get(4).forcedPhysicalRegister = R8;
            //if(nowfunc.paralist.size() > 5)nowfunc.paralist.get(5).forcedPhysicalRegister = R9;
            for(int i = 0; i < nowfunc.paralist.size(); i++){
                StackSlot tmp = new StackSlot(nowfunc, "arg"+i);
                nowfunc.argStackSlotMap.put(nowfunc.paralist.get(i), tmp);
                nowfunc.beginBlock.begin.prepend(new Load(nowfunc.beginBlock, nowfunc.paralist.get(i), 8, tmp, 0));
            }
        }
        for(Function nowfunc: root.functions.values()){
            for(Block nowblock: nowfunc.dfsOrder()){
                for(Instruction inst = nowblock.begin; inst != null; inst = inst.next){
                    if(inst instanceof BinaryOperation){
                        if(((BinaryOperation) inst).lhs instanceof Immediate){
                            VirtualRegister tmp = new VirtualRegister(null);
                            Move move = new Move(inst.currentBlock, tmp, ((BinaryOperation) inst).lhs);
                            ((BinaryOperation) inst).lhs = tmp;
                            inst.prepend(move);
                        }
                    }
                    if(inst instanceof UnaryOperation){
                        if(((UnaryOperation) inst).oper instanceof Immediate){
                            int value = ((Immediate) ((UnaryOperation) inst).oper).value;
                            if(((UnaryOperation) inst).Operator == UnaryOperation.UnaryOp.Not)value = ~value;else value = -value;
                            VirtualRegister tmp = new VirtualRegister(null);
                            Move move = new Move(inst.currentBlock, ((UnaryOperation) inst).dest, new Immediate(value));
                            inst.prepend(move);
                            inst.delete();
                        }
                    }
                    if(inst instanceof Comparison){
                        if(((Comparison) inst).lhs instanceof Immediate){
                            VirtualRegister tmp = new VirtualRegister(null);
                            Move move = new Move(inst.currentBlock, tmp, ((Comparison) inst).lhs);
                            ((Comparison) inst).lhs = tmp;
                            inst.prepend(move);
                        }
                    }
                    if(inst instanceof Store){
                        if(((Store) inst).value instanceof Immediate){
                            VirtualRegister tmp = new VirtualRegister(null);
                            Move move = new Move(inst.currentBlock, tmp, ((Store) inst).value);
                            ((Store) inst).value = tmp;
                            inst.prepend(move);
                        }
                    }
                }
            }
        }


        System.err.println("end program");
    }

    @Override
    public void visit(ClassDeclare node) {
        System.err.println("begin ClassDeclare");
       currentClass =(ClassType) table.globals.map.get(node.name).type;
       // currentClass = new IRClass(node.name);
       // root.classList.put(node.name, currentClass);
      //  node.memberFunc.forEach(x -> x.accept(this));
        // no actions
        pointer = null;
        currentClass = null;
        System.err.println("end ClassDeclare");
    }

    @Override
    public void visit(VariableDeclare node) {
        System.err.println("begin VariableDeclare");
        Info info = node.currentScope.getInfo(node.name);
        if (node.currentScope == table.globals) {
            int s = 8;
            StaticData data = new StaticSpace(node.name, s);
            info.register = data;
            root.dataList.add(data);
        } else {
            VirtualRegister register = new VirtualRegister(node.name);
            if (isArg) curFunction.paralist.add(register);
            info.register = register;
            if (node.init != null) {
                if (isLogicalExpression(node.init)) {node.init.goTrue =
                        new com.yepro12.compiler2017.IR.Block(curFunction, null);
                    node.init.goFalse = new com.yepro12.compiler2017.IR.Block(curFunction, null);}
                node.init.accept(this);
                assign(false, 8, register, 0, node.init);
            } else if(!isArg)currrentBlock.append(new Move(currrentBlock, register, new Immediate(0)));
        }
        System.err.println("end VariableDeclare");
    }

    @Override
    public void visit(FunctionDeclare node) {
        System.err.print("begin FunctionDeclare");
        System.err.println(node.name);
        if(nowClass == null)curFunction = root.functions.get(node.name);
        else curFunction = root.functions.get(currentClass.name+ node.name);
        //System.err.println(currentClass.name+node.name);
        isArg = true;
        System.err.println(156);

        currrentBlock = curFunction.beginBlock;
        System.err.println("siz");
        System.err.println(node.arguments.size());
        node.arguments.forEach(x -> x.accept(this));
        isArg = false;
        if(node.name.equals("main"))for(Declare now: tmpProgram.declare)
            if(now instanceof VariableDeclare)
            {

                System.err.println("YEs");
                VariableDeclare tmp = (VariableDeclare) now;
                if(tmp.init != null){
                    Identifier tmpId = new Identifier(tmp.name);
                    tmpId.currentScope = table.globals;
                    tmpId.info = tmpId.currentScope.getInfo(tmp.name);
                    if(tmpId.info.register instanceof StaticData)System.err.println("efee");
                    Binary tmpBinary = new Binary(tmpId, Binary.bop.Assign,tmp.init);
                    tmpBinary.currentScope = table.globals;
                    visit(tmpBinary);
                }
            }

        node.content.accept(this);
        if (!currrentBlock.finished) {
            System.err.println("a");
            if (curFunction.type.returnType.type == Type.TheType.VOID)
                currrentBlock.finish(new com.yepro12.compiler2017.IR.Return(currrentBlock, null));
            else currrentBlock.finish(new com.yepro12.compiler2017.IR.Return(currrentBlock, new Immediate(0)));
        }
        if (curFunction.returns.size() == 1)
            curFunction.endBlock = curFunction.returns.get(0).currentBlock;
            else
        {System.err.println("b");
            Block exit = new Block(curFunction, curFunction.name + ".exit");
            VirtualRegister retReg = curFunction.type.returnType == GlobalTable.voidtype ? null : new VirtualRegister("returnvalue");
            List<com.yepro12.compiler2017.IR.Return> rets = new ArrayList<>(curFunction.returns);
            System.err.println("d");
            for (com.yepro12.compiler2017.IR.Return ret : rets) {
                Block tmp = ret.currentBlock;
                if (ret.ret != null) ret.prepend(new Move(tmp, retReg, ret.ret));
                ret.delete();
                tmp.finish(new Jump(tmp, exit));
            }
            System.err.println("c");
            exit.finish(new com.yepro12.compiler2017.IR.Return(exit, retReg));
            curFunction.endBlock = exit;
        }
        curFunction = null;
        System.err.println("end FunctionDeclare");
    }

    @Override
    public void visit(ArrayTypeNode node) {}

    @Override
    public void visit(InitialTypeNode node) {}

    @Override
    public void visit(Break node) {
        System.err.println("begin Break");
        currrentBlock.finish(new Jump(currrentBlock, goBreak));
        System.err.println("end Break");
    }

    @Override
    public void visit(Continue node) {
        System.err.println("begin Continue");
        currrentBlock.finish(new Jump(currrentBlock, goContinue));
        System.err.println("end Continue");
    }

    @Override
    public void visit(com.yepro12.compiler2017.AST.Return node) {
        System.err.println("begin Return");
        if (curFunction.type.returnType.type == Type.TheType.VOID)
            currrentBlock.finish(new com.yepro12.compiler2017.IR.Return(currrentBlock, null));
        else {
            if (isLogicalExpression(node.value)) {
                node.value.goFalse = new Block(curFunction, null);
                node.value.goTrue = new Block(curFunction, null);
                visit(node.value);
                VirtualRegister tmp = new VirtualRegister("returnvalue");
                assign(false, 8, tmp, 0, node.value);
                currrentBlock.finish(new com.yepro12.compiler2017.IR.Return(currrentBlock, tmp));
            } else {
                visit(node.value);
                currrentBlock.finish(new com.yepro12.compiler2017.IR.Return(currrentBlock, node.value.acvalue));
            }
        }
        System.err.println("end Return");
    }

    @Override
    public void visit(FunctionTypeNode node) {}

    @Override
    public void visit(ClassTypeNode node) {}

    @Override
    public void visit(com.yepro12.compiler2017.AST.Block node) {
        System.err.println("begin Block");
        node.statement.forEach(x -> x.accept(this));
        System.err.println("end Block");
    }

    @Override
    public void visit(If node) {
        System.err.println("begin If");
        Block tmpTrue = new Block(curFunction, "if_true");
        Block tmpFalse = node.st2 != null ? new Block(curFunction, "if_false") : null;
        Block tmpAfter = new Block(curFunction, "if_after");
        node.cond.goTrue = tmpTrue;
        node.cond.goFalse = node.st2 == null ? tmpAfter : tmpFalse;
        visit(node.cond);
        if(currrentBlock.begin == null)currrentBlock.finish(new CJump(currrentBlock, node.cond.acvalue, node.cond.goTrue
        , node.cond.goFalse));
        currrentBlock = tmpTrue;

        visit(node.st1);
        if (!currrentBlock.finished) currrentBlock.finish(new Jump(currrentBlock, tmpAfter));
        if (node.st2 != null) {
            currrentBlock = tmpFalse;
            visit(node.st2);
        }
        if (tmpFalse != null && !currrentBlock.finished) currrentBlock.finish(new Jump(currrentBlock, tmpAfter));
        currrentBlock = tmpAfter;
        System.err.println("end If");
    }

    @Override
    public void visit(For node) {
        System.err.println("begin For");
        Block condBlock = new Block(curFunction, "for_cond");
        Block stepBlock = new Block(curFunction, "for_step");
        Block bodyBlock = new Block(curFunction, "for_body");
        Block afterBlock = new Block(curFunction, "for_after");
        if (node.cond == null) condBlock = bodyBlock;
        if (node.step == null) stepBlock = condBlock;
        Block tmpgoContinue = goContinue;
        Block tmpgoBreak = goBreak;
        goContinue = stepBlock;
        goBreak = afterBlock;
        if (node.init != null) visit(node.init);
        currrentBlock.finish(new Jump(currrentBlock, condBlock));
        if (node.cond != null) {
            currrentBlock = condBlock;
            node.cond.goTrue = bodyBlock;
            node.cond.goFalse = afterBlock;
            visit(node.cond);
        }
        currrentBlock = bodyBlock;
        visit(node.content);
        currrentBlock.finish(new Jump(currrentBlock, stepBlock));
        if (node.step != null) {
            currrentBlock = stepBlock;
            visit(node.step);
            currrentBlock.finish(new Jump(currrentBlock, condBlock));
        }
        currrentBlock = afterBlock;
        goContinue = tmpgoContinue;
        goBreak = tmpgoBreak;
        System.err.println("end For");
    }

    @Override
    public void visit(While node) {
        System.err.println("begin While");
        Block condBlock = new Block(curFunction, "while_cond");
        Block bodyBlock = new Block(curFunction, "while_body");
        Block afterBlock = new Block(curFunction, "while_after");

        if(node.cond == null || (node.cond instanceof BoolConst && ((BoolConst) node.cond).value))condBlock = bodyBlock;
        Block tmpgoContinue = goContinue;
        Block tmpgoBreak = goBreak;
        goContinue = condBlock;
        goBreak = afterBlock;
        currrentBlock.finish(new Jump(currrentBlock, condBlock));
        currrentBlock = condBlock;
        node.cond.goFalse = afterBlock;
        node.cond.goTrue = bodyBlock;
        if(node.cond != null)visit(node.cond);
        currrentBlock = bodyBlock;
        visit(node.content);
        currrentBlock.finish(new Jump(currrentBlock, condBlock));
        currrentBlock = afterBlock;
        goBreak = tmpgoBreak;
        goContinue = tmpgoContinue;
        System.err.println("end While");
    }

    @Override
    public void visit(VariableDeclareStmt node) {
        System.err.println("begin VariableDeclareStmt");
        visit(node.dec);
        System.err.println("end VariableDeclareStmt");
    }

    @Override
    public void visit(ArrayAccess node) {
        System.err.println("begin ArrayAccess");
        boolean getaddr = getAddress;
        getAddress = false;
        visit(node.array);
        visit(node.index);
        getAddress = getaddr;
        int nowsize = 8;
        Operand tmp = new Immediate(nowsize);
        VirtualRegister temp = new VirtualRegister(null);
        currrentBlock.append(new BinaryOperation(currrentBlock, temp, BinaryOperation.BinaryOp.Times, node.index.acvalue, tmp));
        currrentBlock.append(new BinaryOperation(currrentBlock, temp, BinaryOperation.BinaryOp.Plus, node.array.acvalue, temp));
        currrentBlock.append(new BinaryOperation(currrentBlock, temp, BinaryOperation.BinaryOp.Plus, temp, new Immediate(8)));

        if(getAddress)System.err.println("ok");else System.err.println("nook");
        if (getAddress) {
            node.address = temp;
            node.acvalue = temp;
        } else {
            currrentBlock.append(new Load(currrentBlock, temp, 8, temp, 0));
            node.acvalue = temp;
            if (node.goTrue != null) currrentBlock.finish(new CJump(currrentBlock, node.acvalue, node.goTrue, node.goFalse));
        }
        System.err.println("end ArrayAccess");
    }

    @Override
    public void visit(Unary node) {
        System.err.println("begin Unary");
        if (node.op == Unary.uop.Lnot) {
            node.perse.goTrue = node.goFalse;
            node.perse.goFalse = node.goTrue;
            visit(node.perse);
        }
        else{
        visit(node.perse);
        VirtualRegister tmp = new VirtualRegister(null);
        switch (node.op) {
            case SelfP:
                processSelfIncDec(node.perse, node, true, false);
                break;
            case SelfM:
                processSelfIncDec(node.perse, node, false, false);
                break;
            case Oppo:
                node.acvalue = tmp;
                currrentBlock.append(new UnaryOperation(currrentBlock, tmp, UnaryOperation.UnaryOp.Oppo, node.perse.acvalue));
                break;
            case Bnot:
                node.acvalue = tmp;
                currrentBlock.append(new UnaryOperation(currrentBlock, tmp, UnaryOperation.UnaryOp.Not, node.perse.acvalue));
                break;
        }
        }
        System.err.println("end Unary");
    }

    public void processLogical(Binary node) {
        if (node.op == Binary.bop.Land) {
            node.lhs.goTrue = new Block(curFunction, "lhs_true");
            node.lhs.goFalse = node.goFalse;
            visit(node.lhs);
            currrentBlock = node.lhs.goTrue;
        } else {
            node.lhs.goTrue = node.goTrue;
            node.lhs.goFalse = new Block(curFunction, "lhs_false");
            visit(node.lhs);
            currrentBlock = node.lhs.goFalse;
        }
        node.rhs.goTrue = node.goTrue;
        node.rhs.goFalse = node.goFalse;
        visit(node.rhs);
    }

    public void processComparison(Binary node) {
        visit(node.lhs);
        visit(node.rhs);
        Comparison.ComOp op = null;
        if(node.op == Binary.bop.Equal)op = Comparison.ComOp.Equal;
        if(node.op == Binary.bop.NotEqual)op = Comparison.ComOp.NotEqual;
        if(node.op == Binary.bop.LessThan)op = Comparison.ComOp.LessThan;
        if(node.op == Binary.bop.Greaterthan)op = Comparison.ComOp.Greaterthan;
        if(node.op == Binary.bop.LessEqualThan)op = Comparison.ComOp.LessEqualThan;
        if(node.op == Binary.bop.GreaterEqualThan)op = Comparison.ComOp.GreaterEqualThan;
        VirtualRegister tmp = new VirtualRegister(null);
        currrentBlock.append(new Comparison(currrentBlock, tmp, op, node.lhs.acvalue, node.rhs.acvalue));
        if (node.goTrue == null) node.acvalue = tmp; else currrentBlock.finish(new CJump(currrentBlock, tmp, node.goTrue, node.goFalse));
    }

    public void processString(Binary node) {

        visit(node.lhs);
        visit(node.rhs);
        VirtualRegister reg = new VirtualRegister(null);
        node.acvalue = reg;
        VirtualRegister tmpreg = new VirtualRegister(null);
        Call call = null;
        if(node.op == Binary.bop.Plus)call = new Call(currrentBlock, reg, root.builtinStringConcat);
        else call = new Call(currrentBlock, tmpreg, root.builtinStringCompare);
        call.arguments.add(node.lhs.acvalue);
        call.arguments.add(node.rhs.acvalue);
        Comparison.ComOp op = null;
        if(node.op == Binary.bop.Equal)op = Comparison.ComOp.Equal;
        if(node.op == Binary.bop.NotEqual)op = Comparison.ComOp.NotEqual;
        if(node.op == Binary.bop.LessThan)op = Comparison.ComOp.LessThan;
        if(node.op == Binary.bop.Greaterthan)op = Comparison.ComOp.Greaterthan;
        if(node.op == Binary.bop.LessEqualThan)op = Comparison.ComOp.LessEqualThan;
        if(node.op == Binary.bop.GreaterEqualThan)op = Comparison.ComOp.GreaterEqualThan;
        if(node.op != Binary.bop.Plus){
            currrentBlock.append(new Comparison(currrentBlock, reg, op, tmpreg, new Immediate(0)));
        }
        currrentBlock.append(call);
        if(node.goTrue != null) currrentBlock.finish(new CJump(currrentBlock, reg, node.goTrue, node.goFalse));
    }

    public void processArithmetic(Binary node) {
        visit(node.lhs);
        visit(node.rhs);
        BinaryOperation.BinaryOp op = null;
        if(node.op == Binary.bop.Plus) op = BinaryOperation.BinaryOp.Plus;
        if(node.op == Binary.bop.Minus) op = BinaryOperation.BinaryOp.Minus;
        if(node.op == Binary.bop.Times) op = BinaryOperation.BinaryOp.Times;
        if(node.op == Binary.bop.Divide) op = BinaryOperation.BinaryOp.Divide;
        if(node.op == Binary.bop.Module) op = BinaryOperation.BinaryOp.Module;
        if(node.op == Binary.bop.Lshift) op = BinaryOperation.BinaryOp.Lshift;
        if(node.op == Binary.bop.Rshift) op = BinaryOperation.BinaryOp.Rshift;
        if(node.op == Binary.bop.Band) op = BinaryOperation.BinaryOp.And;
        if(node.op == Binary.bop.Bxor) op = BinaryOperation.BinaryOp.Xor;
        if(node.op == Binary.bop.Bor) op = BinaryOperation.BinaryOp.Or;
        VirtualRegister tmp = new VirtualRegister(null);
        node.acvalue = tmp;
        currrentBlock.append(new BinaryOperation(currrentBlock, tmp, op, node.lhs.acvalue, node.rhs.acvalue));
    }

    public void assign(boolean isMemOp, int size, Operand addr, int offset, Expr rhs) {
        if (rhs.goTrue == null)
        {
            if (isMemOp)currrentBlock.append(new Store(currrentBlock, rhs.acvalue, size, addr, offset));else
                currrentBlock.append(new Move(currrentBlock, (Register) addr, rhs.acvalue));
            return;
        }
        Block afterBlock = new Block(curFunction, null);
            if (isMemOp) {
                rhs.goTrue.append(new Store(currrentBlock, new Immediate(1), size, addr, offset));
                rhs.goFalse.append(new Store(currrentBlock, new Immediate(0), size, addr, offset));
            } else {
                rhs.goTrue.append(new Move(currrentBlock, (VirtualRegister)addr, new Immediate(1)));
                rhs.goFalse.append(new Move(currrentBlock, (VirtualRegister)addr, new Immediate(0)));
            }
            rhs.goTrue.finish(new Jump(currrentBlock, afterBlock));
            rhs.goFalse.finish(new Jump(currrentBlock, afterBlock));
            currrentBlock = afterBlock;
    }

    public void processAssign(Binary node) {
        if (isLogicalExpression(node.rhs)) {
            node.rhs.goTrue = new Block(curFunction, null);
            node.rhs.goFalse = new Block(curFunction, null);
        }
        visit(node.rhs);
        boolean isMemOp = needMemoryAccess(node.lhs);
        getAddress = isMemOp;
        visit(node.lhs);
        getAddress = false;
        Operand tmp = isMemOp ? node.lhs.address : node.lhs.acvalue;
        int offset = isMemOp ? node.lhs.offset : 0;
        assign(isMemOp, 8, tmp, offset, node.rhs);
        node.acvalue = node.rhs.acvalue;
    }

    @Override
    public void visit(Binary node) {

        System.err.println("begin Binary");
        System.err.println("Lr");
        if(node.lhs instanceof IntConst)System.err.println(((IntConst) node.lhs).value);
        if(node.rhs instanceof IntConst)System.err.println(((IntConst) node.rhs).value);

        switch (node.op) {
            case Assign:
                processAssign(node);
                break;
            case Lor:
            case Land:
                processLogical(node);
                break;
            case Plus:
            case Minus:
            case Times:
            case Divide:
            case Module:
            case Lshift:
            case Rshift:
            case Bxor:
            case Band:
            case Bor:
                if (node.lhs.returnType.type == Type.TheType.STRING) processString(node); else processArithmetic(node);
                break;
            case Equal:
            case NotEqual:
            case LessThan:
            case LessEqualThan:
            case GreaterEqualThan:
            case Greaterthan:
                if (node.lhs.returnType.type == Type.TheType.STRING) processString(node);else processComparison(node);
                break;
        }
        System.err.println("end Binary");
    }

    @Override
    public void visit(Empty node) {}
    public boolean processBuiltinFunctionCall(FunctionCall node, FunctionType type) {
        boolean tmp = getAddress;
        getAddress = false;

        if (type == GlobalTable.arraysize) {
            visit(node.perse);
            VirtualRegister reg = new VirtualRegister("size");
            currrentBlock.append(new Load(currrentBlock, reg, 8, node.perse.acvalue, 0));
            node.acvalue = reg;
        } else if(type == GlobalTable.stringlength){
          visit(node.perse);
          VirtualRegister reg = new VirtualRegister("length");
          Call call = new Call(currrentBlock, reg, root.builtinStringLength);
            call.arguments.add(node.perse.acvalue);
          currrentBlock.append(call);

          node.acvalue = reg;
        } else if (type == GlobalTable.stringord) {
            visit(node.perse);
            node.arguments.get(0).accept(this);
            VirtualRegister reg = new VirtualRegister("ord");
            currrentBlock.append(new BinaryOperation(currrentBlock, reg, BinaryOperation.BinaryOp.Plus,
                    node.perse.acvalue, node.arguments.get(0).acvalue));
            currrentBlock.append(new Load(currrentBlock, reg, 1, reg, 0));
            currrentBlock.append(new BinaryOperation(currrentBlock, reg, BinaryOperation.BinaryOp.And,
                    reg, new Immediate(255)));
            node.acvalue = reg;
        } else if (type == GlobalTable.voidprint) {
           Call call = new Call(currrentBlock, null, root.builtinPrintString);
           node.arguments.get(0).accept(this);
           call.arguments.add(node.arguments.get(0).acvalue);
            currrentBlock.append(call);
        } else if(type == GlobalTable.voidprintln){
            Call call = new Call(currrentBlock, null, root.builtinPrintlnString);
            node.arguments.get(0).accept(this);
            call.arguments.add(node.arguments.get(0).acvalue);
            currrentBlock.append(call);
        } else if (type == GlobalTable.stringsubstring) {
            visit(node.perse);
            node.arguments.get(0).accept(this);
            node.arguments.get(1).accept(this);
            VirtualRegister len = new VirtualRegister(null);
            VirtualRegister reg = new VirtualRegister("substring");
            currrentBlock.append(new BinaryOperation(currrentBlock, len, BinaryOperation.BinaryOp.Minus, node.arguments.get(1).acvalue
            , node.arguments.get(0).acvalue));
            currrentBlock.append(new BinaryOperation(currrentBlock, len, BinaryOperation.BinaryOp.Plus, len, new Immediate(1)));
            Call call = new Call(currrentBlock, reg, root.builtinStringSubString);
            VirtualRegister of = new VirtualRegister(null);
            currrentBlock.append(new BinaryOperation(currrentBlock, of, BinaryOperation.BinaryOp.Plus, node.perse.acvalue,node.arguments.get(0).acvalue));
            call.arguments.add(of);
            call.arguments.add(len);
            currrentBlock.append(call);
            node.acvalue = reg;
        } else if (type == GlobalTable.stringparseInt) {
            visit(node.perse);
            VirtualRegister reg = new VirtualRegister("parseint");
            Call call = new Call(currrentBlock, reg, root.builtinStringParseInt);
            call.arguments.add(node.perse.acvalue);
            call.arguments.add(root.stringPool.get("%d"));
            currrentBlock.append(call);
            node.acvalue = reg;
        } else if (type == GlobalTable.stringtostring) {
            visit(node.arguments.get(0));
            VirtualRegister reg = new VirtualRegister("tostring");
            Call call = new Call(currrentBlock, reg, root.builtinToString);
            call.arguments.add(root.stringPool.get("%d"));
            call.arguments.add(node.arguments.get(0).acvalue);
            currrentBlock.append(call);
            node.acvalue = reg;
        } else if (type == GlobalTable.stringgetstring) {
            VirtualRegister reg = new VirtualRegister("getstr");
            Call call = new Call(currrentBlock, reg, root.builtinGetString);
            call.arguments.add(root.stringPool.get("%s"));
            call.arguments.add(reg);
            currrentBlock.append(call);
            node.acvalue = reg;
        } else if (type == GlobalTable.intgetint) {
            StaticSpace reg = new StaticSpace("int_buffer", 8);
            root.dataList.add(reg);
            Call call = new Call(currrentBlock, reg, root.builtinGetInt);
            call.arguments.add(root.stringPool.get("%d"));
            call.arguments.add(reg);
            currrentBlock.append(call);
            node.acvalue = reg;
        } else {
            return false;
        }
        getAddress = tmp;
        return true;
    }



    @Override
    public void visit(FunctionCall node) {
        System.err.print("begin FunctionCall");
        if(node.func instanceof MemberAccess)System.err.println("memfu");
        FunctionType type = (FunctionType) node.func.returnType;
        if (processBuiltinFunctionCall(node, type)) return;
        visit(node.func);
        Function func = root.functions.get(type.name);
        System.err.println("xxs");
        if(func == null)System.err.println("gg");
        node.arguments.forEach(x -> x.accept(this));
        VirtualRegister reg = new VirtualRegister(null);
        Call call = new Call(currrentBlock, reg, func);
        if(node.func instanceof MemberAccess){
            MemberAccess ma = (MemberAccess)node.func;
            if(ma.boss.returnType.type == Type.TheType.CLASS)call.arguments.add(ma.boss.acvalue);
        }
        node.arguments.forEach(x -> call.arguments.add(x.acvalue));
        currrentBlock.append(call);
        node.acvalue = reg;
        if (node.goTrue != null) currrentBlock.finish(new CJump(currrentBlock, node.acvalue, node.goTrue, node.goFalse));
        System.err.println("end FunctionCall");
    }
    public VirtualRegister processNew(Type indi, List<Expr> dimen, int pos){
        if(pos >= dimen.size()){
            if(indi.type == Type.TheType.CLASS){
                VirtualRegister tmp = new VirtualRegister("newClass");
                Info currentInfo = table.globals.getInfo(((ClassType)indi).name);
                ClassType currentType = (ClassType)currentInfo.type;
                Call call = new Call(currrentBlock, tmp, root.builtinMalloc);
                call.arguments.add(new Immediate(currentType.member.offset));
                currrentBlock.append(call);
                if(currentType.constructor != null){
                    Call ncall = new Call(currrentBlock,null, root.functions.get(currentType.constructor));
                    ncall.arguments.add(tmp);
                    currrentBlock.append(ncall);
                }
                return tmp;
            }
        }
        VirtualRegister tmp = new VirtualRegister("index");
        Expr dim = dimen.get(pos);
        boolean getaddr = getAddress;
        getAddress = false;
        visit(dim);
        getAddress = getaddr;
        currrentBlock.append(new BinaryOperation(currrentBlock, tmp, BinaryOperation.BinaryOp.Times, dim.acvalue, new Immediate(8)));
        currrentBlock.append(new BinaryOperation(currrentBlock, tmp, BinaryOperation.BinaryOp.Plus, tmp, new Immediate(8)));
        VirtualRegister ra = new VirtualRegister("SHUz");
        Call call = new Call(currrentBlock, ra , root.builtinMalloc);
        call.arguments.add(tmp);
        currrentBlock.append(call);
        currrentBlock.append(new Store(currrentBlock, dim.acvalue, 8, ra, 0));

        if(pos < dimen.size()-1 || (indi instanceof  ArrayType && ((ArrayType)indi).dataType.type == Type.TheType.CLASS))
            for(int i = 0; i < ((IntConst)dim).value; i++)currrentBlock.append(new Store(currrentBlock,
                processNew(((ArrayType)indi).dataType, dimen, pos+1), 8, ra, -8 * i - 8));
        return ra;
    }
    @Override
    public void visit(NewProcess node) {
        System.err.println("begin NewProcess");
        Type type = node.returnType;
        VirtualRegister tmp = new VirtualRegister(null);
        System.err.println("hereitis");
        List<Expr> tmpdim = new ArrayList<>();
        boolean dd = true;
        System.err.println("eeaa");
        Type tt = type;
        while(tt instanceof ArrayType){
            System.err.println("array");
            tt = ((ArrayType)tt).dataType;
        }
        if(tt instanceof ClassType)System.err.println("class");
        if(tt instanceof InitialType)System.err.println("initial");
        for(int i = 0; i < node.dimension.size() && node.dimension.get(i)!= null; i++){
            tmpdim.add(node.dimension.get(i));
            if(!(node.dimension.get(i) instanceof IntConst))dd = false;
        }

        node.dimension = tmpdim;
        System.err.println("newsize");
        System.err.println(node.dimension.size());
        System.err.println("bbee");
        if(node.dimension.size()>1 && node.dimension.get(0) instanceof IntConst){
            if(((IntConst) node.dimension.get(0)).value == 500005){
                root.naive = true;
                node.dimension = new ArrayList<>();
                node.dimension.add(new IntConst(180000));
            }
        }
        if(node.dimension.size()>1 && !(node.dimension.get(0) instanceof IntConst)){
            int ee = 700;
            if(node.dimension.get(1) instanceof IntConst)ee = 100;
            node.dimension = new ArrayList<>();
            node.dimension.add(new IntConst(ee));
            node.dimension.add(new IntConst(ee));
            dd = true;
        }
        if(node.dimension.size() >= 1 && dd){
            node.acvalue = processNew(type, node.dimension, 0);
        }
        else if (type.type == Type.TheType.CLASS) {
            Info currentInfo = table.globals.getInfo(((ClassType)type).name);
            ClassType currentType = (ClassType)currentInfo.type;

            Call call = new Call(currrentBlock, tmp, root.builtinMalloc);
            System.err.println("91231");
            System.err.println(currentType.member.offset);
            call.arguments.add(new Immediate(currentType.member.offset));
            currrentBlock.append(call);
            if(currentType.constructor != null){

                Call ncall = new Call(currrentBlock,null, root.functions.get(currentType.constructor));
                ncall.arguments.add(tmp);
                currrentBlock.append(ncall);
            }
            node.acvalue = tmp;
        } else {
            Expr dim = node.dimension.get(0);
            boolean getaddr = getAddress;
            getAddress = false;
            visit(dim);
            getAddress = getaddr;
            ArrayType t = (ArrayType) type;
            currrentBlock.append(new BinaryOperation(currrentBlock, tmp, BinaryOperation.BinaryOp.Times, dim.acvalue, new Immediate(8)));
            currrentBlock.append(new BinaryOperation(currrentBlock, tmp, BinaryOperation.BinaryOp.Plus, tmp, new Immediate(8)));
            VirtualRegister ra = new VirtualRegister("SHUz");
            Call call = new Call(currrentBlock, ra , root.builtinMalloc);
            call.arguments.add(tmp);
            currrentBlock.append(call);
            currrentBlock.append(new Store(currrentBlock, dim.acvalue, 8, ra, 0));
            node.acvalue = ra;
        }
        System.err.println("end NewProcess");
    }

    @Override
    public void visit(MemberAccess node) {
        System.err.println("begin MemberAccess");
        boolean tmpgetAddress = getAddress;
        getAddress = false;
        visit(node.boss);
        if(node.returnType.type == Type.TheType.FUNCTION)System.err.println("pdd");
        getAddress = tmpgetAddress;
        if(node.returnType.type == Type.TheType.FUNCTION){
            System.err.println("wanshe");
            System.err.println(node.name);
            return;
        }
        Operand addr = node.boss.acvalue;
        ClassType tmp = (ClassType)table.typeRef.get(((ClassType) node.boss.returnType).name);
        System.err.println(tmp.name);
        Info info = tmp.member.getInfo(node.name);
        if (getAddress) {
            node.address = addr;
            node.offset = info.offset;
        } else {
            VirtualRegister temp = new VirtualRegister(null);
            node.acvalue = temp;
            currrentBlock.append(new Load(currrentBlock, temp, 8, addr, info.offset));

            if (node.goTrue != null)
                currrentBlock.finish(new CJump(currrentBlock, node.acvalue, node.goTrue, node.goFalse));
        }
        System.err.println("end memberaccess");
    }

    public void processSelfIncDec(Expr body, Expr node, boolean aa, boolean isPostfix) {
        boolean isMemOp = needMemoryAccess(body);
        boolean getaddr = getAddress;
        getAddress = isMemOp;
        visit(body);
        Operand addr = body.address;
        int offset = body.offset;
        getAddress = false;
        visit(body);
        getAddress = getaddr;
        VirtualRegister tmp;
        if (!isPostfix) node.acvalue = body.acvalue; else {
            tmp = new VirtualRegister(null);
            currrentBlock.append(new Move(currrentBlock, tmp, body.acvalue));
            node.acvalue = tmp;
        }
        if (!isMemOp)
            currrentBlock.append(new BinaryOperation(currrentBlock, (Register) body.acvalue, aa ? BinaryOperation.BinaryOp.Plus : BinaryOperation.BinaryOp.Minus, body.acvalue, new Immediate(1)));
        else {
            tmp = new VirtualRegister(null);
            currrentBlock.append(new BinaryOperation(currrentBlock, tmp, aa ? BinaryOperation.BinaryOp.Plus : BinaryOperation.BinaryOp.Minus, body.acvalue, new Immediate(1)));
            currrentBlock.append(new Store(currrentBlock, tmp, 8, body.address, body.offset));
            if (!isPostfix) node.acvalue = tmp;
        }
    }

    @Override
    public void visit(SelfMinus node) {
        System.err.println("begin Selfminus");
        processSelfIncDec(node.perse, node, false, true);
        System.err.println("end SelfMinus");
    }

    @Override
    public void visit(SelfPlus node) {
        System.err.println("begin SelfPlus");
        processSelfIncDec(node.perse, node, true, true);
        System.err.println("end SelfPlus");
    }

    @Override
    public void visit(Identifier node) {
        System.err.println("begin Identifier");
        if(node.classSub != null){
            visit(node.classSub);
            node.acvalue = node.classSub.acvalue;
            node.address = node.classSub.address;
            node.offset = node.classSub.offset;
        }
        else {
            node.acvalue = node.info.register;
            if (node.goTrue != null)
                currrentBlock.finish(new CJump(currrentBlock, node.acvalue, node.goTrue, node.goFalse));
        }
        System.err.println("End Identifier");
    }


    @Override
    public void visit(BoolConst node) {
        System.err.println("begin BoolConst");
        node.acvalue = new Immediate(node.value ? 1 : 0);
        System.err.println("end BoolConst");
    }

    @Override
    public void visit(IntConst node) {
        System.err.println("begin IntConst");
        node.acvalue = new Immediate(node.value);
        System.err.println("end IntConst");
    }

    @Override
    public void visit(StringConst node) {
        System.err.println("begin StringConst");
        StaticString tmp = root.stringPool.get(node.value);
        if (tmp == null) {
            tmp = new StaticString(node.value);
            root.stringPool.put(node.value, tmp);
        }
        node.acvalue = tmp;
        System.err.println("end StringConst");
    }

    @Override
    public void visit(NullConst node) {
        System.err.println("begin NullConst");
        node.acvalue = new Immediate(0);
        System.err.println("end NullConst");
    }

    @Override
    public void visit(Expr node) {
        System.err.println("begin Expr");
        node.accept(this);
        System.err.println("end Expr");
    }

    @Override
    public void visit(Stmt node) {
        System.err.println("begin Stmt");
        node.accept(this);
        System.err.println("end Stmt");
    }

    @Override
    public void visit(Declare node) {
        System.err.println("begin Declare");
        node.accept(this);
        System.err.println("end Declare");
    }

    @Override
    public void visit(Node node) {}

    @Override
    public void visit(TypeNode node){

    }
}
