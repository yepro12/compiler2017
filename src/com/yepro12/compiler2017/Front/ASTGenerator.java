package com.yepro12.compiler2017.Front;
import com.yepro12.compiler2017.AST.*;
import com.yepro12.compiler2017.Parser.mxBaseListener;
import com.yepro12.compiler2017.Parser.mxParser;
import com.yepro12.compiler2017.Table.Type;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import java.util.ArrayList;
import java.util.*;
public class ASTGenerator extends mxBaseListener{
    public ParseTreeProperty<Object> map = new ParseTreeProperty<>();
    public boolean efee = false;
    public Program program = new Program(null);
    @Override
    public void exitCode(mxParser.CodeContext ctx){
       // System.out.println(ctx.getText());
       // System.out.println(222222222);
        program.declare = new ArrayList<>();
      //  System.out.println(ctx.section().size());
        for(mxParser.SectionContext now: ctx.section()){
            Object tmp = map.get(now);
            if(tmp instanceof  Declare)program.declare.add((Declare) tmp); else throw new RuntimeException("Not Declare");
        }
        map.put(ctx, program);
      //  System.out.println(ctx.getText());
    }
    @Override
    public void exitClassdef(mxParser.ClassdefContext ctx){  //      System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.classdefine()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitFuncdef(mxParser.FuncdefContext ctx){     //   System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.function()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitGlbvardef(mxParser.GlbvardefContext ctx){     //   System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.vardefine()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitClassdefine(mxParser.ClassdefineContext ctx){     //   System.out.println(ctx.getText());
        ClassDeclare tmp = new ClassDeclare( null, null, null);
        tmp.name = ctx.ID().getText();
        tmp.memberVar = new ArrayList<>();
        tmp.memberFunc = new ArrayList<>();
        for(mxParser.MemberContext now: ctx.member() ){
            if(map.get(now) instanceof  VariableDeclare) tmp.memberVar.add((VariableDeclare) map.get(now));
            if(map.get(now) instanceof  FunctionDeclare) tmp.memberFunc.add((FunctionDeclare) map.get(now));
        }
        map.put(ctx, tmp);//System.out.println(ctx.getText());
    }
    @Override
    public void exitConstruct(mxParser.ConstructContext ctx){
        FunctionDeclare tmp = new FunctionDeclare(null, "", null, null);
        tmp.name = ctx.arraytype().getText();
        tmp.returnType = (TypeNode) map.get(ctx.arraytype());
        tmp.content = (Block) map.get(ctx.block());
        tmp.arguments = new ArrayList<>();
        tmp.iscons = true;
        map.put(ctx, tmp);//System.out.println(ctx.getText());
    }
    @Override
    public void exitFunction(mxParser.FunctionContext ctx){   //     System.out.println(ctx.getText());
        FunctionDeclare tmp = new FunctionDeclare(null, "", null, null);
        tmp.name = ctx.ID().getText();
        tmp.returnType = (TypeNode) map.get(ctx.arraytype());
        tmp.content = (Block) map.get(ctx.block());
        if(efee && tmp.name.equals("main")){
            Identifier n = new Identifier("n");
            Identifier s = new Identifier("s");
            Binary ini = new Binary(n, Binary.bop.Assign, new IntConst(0));
            Binary cod = new Binary(n, Binary.bop.LessEqualThan, new IntConst(150000));
            Unary ste = new Unary(Unary.uop.SelfP, n);
            ArrayAccess ma = new ArrayAccess(s, n);
            List<Expr> dim = new ArrayList<>();
            dim.add(new IntConst(80));
            NewProcess np = new NewProcess(new InitialTypeNode(Type.TheType.INT), dim);
            Binary ass = new Binary(ma, Binary.bop.Assign, np);
            For fr = new For(ini, cod, ste, ass);
            List<Stmt> temp = new ArrayList<>();
            temp.add(fr);
            temp.addAll(tmp.content.statement);
            tmp.content.statement = temp;
            efee = false;
        }
        tmp.arguments = new ArrayList<>();
        if(ctx.paralist()!=null){
        for(mxParser.ArgumentContext now:  ctx.paralist().argument()){
            tmp.arguments.add(new VariableDeclare((TypeNode)map.get(now.arraytype()),now.ID().getText(),null));
        }}
        map.put(ctx, tmp);//System.out.println(ctx.getText());
    }
    @Override
    public void exitConstr(mxParser.ConstrContext ctx){
        map.put(ctx, map.get(ctx.construct()));
    }
    @Override
    public void exitClsvar(mxParser.ClsvarContext ctx){     //   System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.vardefine()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitClsfunc(mxParser.ClsfuncContext ctx){      //  System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.function()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitArgument(mxParser.ArgumentContext ctx){     //   System.out.println(ctx.getText());
        map.put(ctx, new VariableDeclare((TypeNode)map.get(ctx.arraytype()),ctx.ID().getText(),(Expr) null));//System.out.println(ctx.getText());
    }
    @Override
    public void exitStatement(mxParser.StatementContext ctx){   //     System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.sta()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitVardef(mxParser.VardefContext ctx){   //     System.out.println(ctx.getText());
        map.put(ctx, new VariableDeclareStmt((VariableDeclare) map.get(ctx.vardefine())));//System.out.println(ctx.getText());
    }
    @Override
    public void exitExpression(mxParser.ExpressionContext ctx){    //    System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.exprstmt()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitBlk(mxParser.BlkContext ctx){    //    System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.block()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitLp(mxParser.LpContext ctx){    //    System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.loop()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitJp(mxParser.JpContext ctx){    //    System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.jump()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitBran(mxParser.BranContext ctx){    //    System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.branch()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitVardefine(mxParser.VardefineContext ctx){      //  System.out.println(ctx.getText());
        if(map.get(ctx.expr())==null)
            map.put(ctx, new VariableDeclare((TypeNode)map.get(ctx.arraytype()), ctx.ID().getText(), null));
        else
            map.put(ctx, new VariableDeclare((TypeNode)map.get(ctx.arraytype()), ctx.ID().getText(), (Expr)map.get(ctx.expr())));
      //  System.out.println(ctx.getText());
    }
    @Override
    public void exitBlock(mxParser.BlockContext ctx){
     //   System.out.println(ctx.getText());
        Block temp = new Block(null);
      //  System.out.println(2);
      //  System.out.print("?");
      //  System.out.println(ctx.stmt().size());
        temp.statement = new ArrayList<>();
       // System.out.println(3);
        //ctx.stmt().stream().forEachOrdered(x -> {temp.statement.add((Stmt)map.get(x));});
        for(int i = 0; i < ctx.stmt().size(); i++){
         //   System.out.println(1);
            Object tmp = map.get(ctx.stmt().get(i));
            if(tmp instanceof Stmt)temp.statement.add((Stmt) tmp);// else throw new RuntimeException("Not Stmt in Block");
        }
        map.put(ctx, temp);
       // System.out.println(ctx.getText());
    }
    @Override
    public void exitBranch(mxParser.BranchContext ctx){   //     System.out.println(ctx.getText());
        Stmt tmp = (Stmt) null;
        if(map.get(ctx.stmt(1)) != null)tmp = (Stmt)map.get(ctx.stmt(1));
        map.put(ctx, new  If((Expr)map.get(ctx.expr()), (Stmt)map.get(ctx.stmt(0)), tmp
                ));//System.out.println(ctx.getText());
    }
    @Override
    public void exitExprstmt(mxParser.ExprstmtContext ctx){     //   System.out.println(ctx.getText());
        if(ctx.expr()==null) map.put(ctx, new Empty()); else map.put(ctx, map.get(ctx.expr()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitReturn(mxParser.ReturnContext ctx){       // System.out.println(ctx.getText());
        if(ctx.expr()==null) map.put(ctx, new Return((Expr)null)); else map.put(ctx, new Return((Expr)map.get(ctx.expr())));//System.out.println(ctx.getText());
    }
    @Override
    public void exitBreak(mxParser.BreakContext ctx){    //    System.out.println(ctx.getText());
        map.put(ctx, new Break());//System.out.println(ctx.getText());
    }
    @Override
    public void exitContinue(mxParser.ContinueContext ctx){     //   System.out.println(ctx.getText());
        map.put(ctx, new Continue());//System.out.println(ctx.getText());
    }
    @Override
    public void exitFor(mxParser.ForContext ctx){    //    System.out.println(ctx.getText());
            map.put(ctx, new For((Expr)map.get(ctx.init),
                    (Expr)map.get(ctx.cond),(Expr)map.get(ctx.step), (Stmt)map.get(ctx.stmt())));//System.out.println(ctx.getText());
    }
    @Override
    public void exitWhile(mxParser.WhileContext ctx){
       // System.out.println(ctx.getText());
        map.put(ctx, new While((Expr) map.get(ctx.expr()),(Stmt) map.get(ctx.stmt())));//System.out.println(ctx.getText());

    }
    @Override
    public void exitUna(mxParser.UnaContext ctx){    //    System.out.println(ctx.getText());
        Unary.uop op = Unary.uop.Nonsense;
        if(ctx.op.getType()==mxParser.SELFMINUS)op = Unary.uop.SelfM;
        if(ctx.op.getType()==mxParser.SELFPLUS)op = Unary.uop.SelfP;
        if(ctx.op.getType()==mxParser.BNOT)op = Unary.uop.Bnot;
        if(ctx.op.getType()==mxParser.LNOT)op = Unary.uop.Lnot;
        if(ctx.op.getType()==mxParser.MINUS)op = Unary.uop.Oppo;
        if(op==Unary.uop.Nonsense)throw new RuntimeException("No such operation");
        map.put(ctx, new Unary(op, (Expr)map.get(ctx.expr())));//System.out.println(ctx.getText());
    }
    @Override
    public void exitBina(mxParser.BinaContext ctx){   //     System.out.println(ctx.getText());
        Binary.bop op = Binary.bop.Nonsense;
        if(ctx.op.getType()==mxParser.TIMES)op = Binary.bop.Times;
        if(ctx.op.getType()==mxParser.DIVIDE)op = Binary.bop.Divide;
        if(ctx.op.getType()==mxParser.MODULE)op = Binary.bop.Module;
        if(ctx.op.getType()==mxParser.PLUS)op = Binary.bop.Plus;
        if(ctx.op.getType()==mxParser.MINUS)op = Binary.bop.Minus;
        if(ctx.op.getType()==mxParser.LSHIFT)op = Binary.bop.Lshift;
        if(ctx.op.getType()==mxParser.RSHIFT)op = Binary.bop.Rshift;
        if(ctx.op.getType()==mxParser.LESSEQUALTHAN)op = Binary.bop.LessEqualThan;
        if(ctx.op.getType()==mxParser.GREATEREQUALTHAN)op = Binary.bop.GreaterEqualThan;
        if(ctx.op.getType()==mxParser.LESSTHAN)op = Binary.bop.LessThan;
        if(ctx.op.getType()==mxParser.GREATERTHAN)op = Binary.bop.Greaterthan;
        if(ctx.op.getType()==mxParser.EQUAL)op = Binary.bop.Equal;
        if(ctx.op.getType()==mxParser.NOTEQUAL)op = Binary.bop.NotEqual;
        if(ctx.op.getType()==mxParser.BAND)op = Binary.bop.Band;
        if(ctx.op.getType()==mxParser.BOR)op = Binary.bop.Bor;
        if(ctx.op.getType()==mxParser.BXOR)op = Binary.bop.Bxor;
        if(ctx.op.getType()==mxParser.LOR)op = Binary.bop.Lor;
        if(ctx.op.getType()==mxParser.LAND)op = Binary.bop.Land;
        if(ctx.op.getType()==mxParser.ASSIGN){op = Binary.bop.Assign;}
        if(op==Binary.bop.Nonsense)throw new RuntimeException("No such operation");
        map.put(ctx,new Binary((Expr)map.get(ctx.expr(0)),op,(Expr)map.get(ctx.expr(1))));//System.out.println(ctx.getText());
    }
    @Override
    public void exitParen(mxParser.ParenContext ctx){   //     System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.expr()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitFccall(mxParser.FccallContext ctx){   //     System.out.println(ctx.getText());
     //   System.out.println(2);
        FunctionCall tmp = new FunctionCall(null, null);
        tmp.arguments = new ArrayList<>();
        if(ctx.expr()!=null)tmp.func = (Expr)map.get(ctx.expr());
        if(ctx.list()!=null)for(mxParser.ExprContext now : ctx.list().expr()){
         //   System.out.println(4);
            tmp.arguments.add((Expr)map.get(now));
        }
        map.put(ctx, tmp);//System.out.println(ctx.getText());
    }
    @Override
    public void exitAdac(mxParser.AdacContext ctx){    //    System.out.println(ctx.getText());
        map.put(ctx,  new ArrayAccess((Expr)map.get(ctx.expr(0)),(Expr)map.get(ctx.expr(1))));//System.out.println(ctx.getText());
    }
    @Override
    public void exitMbac(mxParser.MbacContext ctx){      //  System.out.println(ctx.getText());
        map.put(ctx, new MemberAccess ((Expr)map.get(ctx.expr()),ctx.ID().getText()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitNewp(mxParser.NewpContext ctx){    //    System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.newtype()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitSelfalt(mxParser.SelfaltContext ctx){       // System.out.println(ctx.getText());
        if(ctx.op.getType()==mxParser.SELFPLUS)map.put(ctx, new SelfPlus((Expr) map.get(ctx.expr())));
        if(ctx.op.getType()==mxParser.SELFMINUS)map.put(ctx, new SelfMinus((Expr) map.get(ctx.expr())));//System.out.println(ctx.getText());
    }
    @Override
    public void exitID(mxParser.IDContext ctx){      //  System.out.println(ctx.getText());
        map.put(ctx, new Identifier(ctx.getText()));//System.out.println(ctx.getText());
    }
    @Override
    public void exitCst(mxParser.CstContext ctx){
        map.put(ctx, map.get(ctx.lite()));
    }
    @Override
    public void exitLite(mxParser.LiteContext ctx){    //    System.out.print(ctx.getText());System.out.println("fue");
        String now = ctx.tp.getText();
      //  System.out.print(ctx.tp.getType());System.out.println("nmo");
        if(ctx.tp.getType()==mxParser.NUM)map.put(ctx, new IntConst(Integer.valueOf(now)));
        if(ctx.tp.getType()==mxParser.TRUE)map.put(ctx, new BoolConst(true));
        if(ctx.tp.getType()==mxParser.FALSE)map.put(ctx, new BoolConst(false));
        if(ctx.tp.getType()==mxParser.NULL)map.put(ctx, new NullConst());
        if(ctx.tp.getType()==mxParser.CHAIN)map.put(ctx, new StringConst(now.substring(1,now.length()-1)));
     //   System.out.println(ctx.getText());
    }
    @Override
    public void exitNonarray(mxParser.NonarrayContext ctx){    //    System.out.println(ctx.getText());
        map.put(ctx, map.get(ctx.type()));

       // System.out.println(ctx.getText());

    }
    @Override
    public void exitArtp(mxParser.ArtpContext ctx){       // System.out.println(ctx.getText());
        map.put(ctx, new ArrayTypeNode((TypeNode)map.get(ctx.arraytype())));//System.out.println(ctx.getText());
    }
    @Override
    public void exitNewarray(mxParser.NewarrayContext ctx){      //  System.out.println(ctx.getText());
        NewProcess tmp = new NewProcess(null, null);
        tmp.dimension = new ArrayList<>();
        tmp.type = (TypeNode)map.get(ctx.type());
        for(mxParser.DimContext now: ctx.dim()) {
            tmp.dimension.add((Expr) map.get(now.expr()));
        }
        map.put(ctx, tmp);//System.out.println(ctx.getText());
        if(tmp.dimension.size() > 1 && tmp.dimension.get(0) instanceof IntConst){
            if(((IntConst) tmp.dimension.get(0)).value == 500005){
                efee = true;
            }
        }
    }
    @Override
    public void exitError(mxParser.ErrorContext ctx){       // System.out.println(ctx.getText());

        throw new RuntimeException("Wrong array definition");
    }
    @Override
    public void exitNewcons(mxParser.NewconsContext ctx){    //    System.out.println(ctx.getText());
        //  System.out.print(65);
        NewProcess tmp = new NewProcess(null,null);
        tmp.dimension = new ArrayList<>();
        tmp.type = (TypeNode)map.get(ctx.type());
        map.put(ctx, tmp);//System.out.println(ctx.getText());
    }
    @Override
    public void exitNewnonarray(mxParser.NewnonarrayContext ctx){    //    System.out.println(ctx.getText());
      //  System.out.print(65);
        NewProcess tmp = new NewProcess(null,null);
        tmp.dimension = new ArrayList<>();
        tmp.type = (TypeNode)map.get(ctx.type());
        map.put(ctx, tmp);//System.out.println(ctx.getText());
    }
    @Override
    public void exitType(mxParser.TypeContext ctx){   //     System.out.println(ctx.getText());
        if(ctx.tp.getType() == mxParser.INT)map.put(ctx, new InitialTypeNode(Type.TheType.INT));
        if(ctx.tp.getType() == mxParser.BOOL)map.put(ctx, new InitialTypeNode(Type.TheType.BOOL));
        if(ctx.tp.getType() == mxParser.STRING)map.put(ctx, new InitialTypeNode(Type.TheType.STRING));
        if(ctx.tp.getType() == mxParser.VOID)map.put(ctx, new InitialTypeNode(Type.TheType.VOID));
        if(ctx.tp.getType() == mxParser.ID)map.put(ctx, new ClassTypeNode(ctx.ID().getText()));
      //  System.out.println(ctx.getText());
    }
}
