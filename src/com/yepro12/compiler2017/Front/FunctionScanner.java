package com.yepro12.compiler2017.Front;
import com.yepro12.compiler2017.AST.*;
import com.yepro12.compiler2017.IR.VirtualRegister;
import com.yepro12.compiler2017.Table.*;
import java.util.*;


/**
 * Created by yepro12 on 2017/4/4.
 */
public class FunctionScanner implements ASTVisitor{
    public GlobalTable global;
    public boolean Binarycheck(Expr now, List<VariableDeclare> nowdec){
        if(now instanceof Binary){
            return Binarycheck(((Binary) now).lhs, nowdec) && Binarycheck(((Binary) now).rhs, nowdec);
        }
        if(now instanceof IntConst)return true;
        if(now instanceof Identifier){
            for(int i = 0; i < nowdec.size(); i++)if(nowdec.get(i).name.equals(((Identifier) now).name))return true;
                return false;
        }
        return false;
    }
    public FunctionScanner(GlobalTable global){
        this.global = global;
    }
    @Override
    public void visit(ArrayAccess node){
    }
    @Override
    public void visit(Binary node){
    }
    @Override
    public void visit(Block node){
    }
    @Override
    public void visit(BoolConst node){
    }
    @Override
    public void visit(Break node){
    }
    @Override
    public void visit(ClassDeclare node){
        ClassType nowtype =  (ClassType) global.typeRef.get(node.name);
        for(VariableDeclare now: node.memberVar){
            if(nowtype.member.map.get(now.name)==null){
                if(now.name.equals("this"))throw new RuntimeException("No define this");
                if(now.init!=null)throw new RuntimeException("No init here");
                VariableType ttype = global.getType(now.variableType);
                if(ttype==null)throw new RuntimeException("Cannot resolve the vartype");
                nowtype.member.map.put(now.name, new Info(ttype, nowtype.member.offset));
                nowtype.member.offset -= 8;
            }else throw new RuntimeException("Same member var");
        }
        nowtype.member.offset *= -1;
        for(FunctionDeclare now: node.memberFunc){
            String tmpname = now.name;
            if(nowtype.member.map.get(now.name)==null){
                if(now.name.equals("this"))throw new RuntimeException("Cannot define this");
                if(global.getType(now.returnType)==null)
                    throw new RuntimeException("cannot resolve functype");
             //   System.out.println(1234);
                now.name = nowtype.name+"_"+now.name;
                FunctionType ttype = new FunctionType(global.getType(now.returnType), now.name);
                if(now.iscons)ttype = new FunctionType(new InitialType(Type.TheType.VOID), now.name);
                if(now.iscons)nowtype.constructor = now.name;
                //ttype.addArgument(nowtype, "this");
                now.parentClass = nowtype;
                if(now.iscons) {
                    /*System.err.println("transfer");
                    NewProcess np = new NewProcess(new ClassTypeNode(node.name), new ArrayList<>());
                    Identifier newId = new Identifier("this");
                    Binary bnr = new Binary(newId, Binary.bop.Assign, np);
                    List<Stmt> tmpcont = new ArrayList<>();
                    Identifier retId = new Identifier("this");
                    Return rt = new Return(retId);
                    tmpcont.add(bnr);
                    now.content.statement.forEach(x->tmpcont.add(x));
                    tmpcont.add(rt);
                    now.content.statement = tmpcont;*/
                }
                List <VariableDeclare> newArg = new ArrayList<>();
                VariableDeclare varThis = new VariableDeclare(new ClassTypeNode(node.name), "this", null);
                newArg.add(varThis);
                for(int i = 0; i < now.arguments.size(); i++)newArg.add(now.arguments.get(i));
                now.arguments = newArg;
                for(int i = 0; i < now.arguments.size(); i++)ttype.addArgument(global.getType(now.arguments.get(i).variableType),
                        now.arguments.get(i).name);
                nowtype.member.map.put(tmpname, new Info(ttype, nowtype.member.offset));
                global.globals.map.put(now.name, new Info(ttype, nowtype.member.offset));
                now.functype = ttype;
            }else throw new RuntimeException("Same member func");
        }
        global.globals.map.put(node.name, new Info(nowtype, nowtype.member.offset));

    }
    @Override
    public void visit(Continue node){
    }
    @Override
    public void visit(Declare node){
        node.accept(this);
    }
    @Override
    public void visit(Empty node){
    }
    @Override
    public void visit(Expr node){
    }
    @Override
    public void visit(For node){
    }
    @Override
    public void visit(FunctionCall node){
    }
    @Override
    public void visit(FunctionDeclare node){

        if(global.typeRef.get(node.name)!=null)throw new RuntimeException("Change the function name");
        VariableType nowtype = global.getType(node.returnType);
        if(nowtype == null)throw new RuntimeException("No such function type");
     //   System.out.println(node.name);
        if(node.name.equals("main")&&(node.arguments.size()!=0||node.returnType.type!= Type.TheType.INT))throw new RuntimeException("Incorrect Main");
        FunctionType ret = new FunctionType(nowtype, node.name);
        for(VariableDeclare now :node.arguments){
            if(now.init!=null)throw new RuntimeException("No init in func");
            VariableType ttype = global.getType(now.variableType);
            if(ttype==null)throw new RuntimeException("Cannot resolve parameter");
            ret.argumentType.add(ttype);
            ret.argumentName.add(now.name);
        }
        if(node.content.statement.size() == 1){
            if(node.content.statement.get(0) instanceof Return){
                Return rt = (Return)node.content.statement.get(0);
                if(Binarycheck(rt.value, node.arguments)){
                    ret.canbeSub = true;
                    ret.replacement = rt.value;
                }
            }
        }
        node.functype = ret;
        global.globals.map.put(node.name, new Info(ret, global.globals.offset));
        global.globals.offset += 8;
    }
    @Override
    public void visit(Identifier node){
    }
    @Override
    public void visit(If node){
    }
    @Override
    public void visit(IntConst node){
    }
    @Override
    public void visit(MemberAccess node){
    }
    @Override
    public void visit(NewProcess node){
    }
    @Override
    public void visit(Node node){
    }
    @Override
    public void visit(NullConst node){
    }
    @Override
    public void visit(Program node){
        for(Declare now: node.declare)now.accept(this);
    }
    @Override
    public void visit(Return node){
    }
    @Override
    public void visit(SelfMinus node){
    }
    @Override
    public void visit(SelfPlus node){
    }
    @Override
    public void visit(Stmt node){
    }
    @Override
    public void visit(StringConst node){
    }
    @Override
    public void visit(Unary node){
    }
    @Override
    public void visit(VariableDeclare node){
    }
    @Override
    public void visit(VariableDeclareStmt node){
    }
    @Override
    public void visit(While node){
    }
    @Override
    public void visit(TypeNode node){
    }
    @Override
    public void visit(InitialTypeNode node){
    }
    @Override
    public void visit(ArrayTypeNode node){
    }
    @Override
    public void visit(FunctionTypeNode node){
    }
    @Override
    public void visit(ClassTypeNode node){

    }
}
