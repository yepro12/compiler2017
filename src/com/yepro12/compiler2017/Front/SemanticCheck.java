package com.yepro12.compiler2017.Front;
import com.yepro12.compiler2017.AST.*;
import com.yepro12.compiler2017.IR.Function;
import com.yepro12.compiler2017.Table.*;
import java.util.*;
/**
 * Created by yepro12 on 2017/4/4.
 */
public class SemanticCheck implements ASTVisitor{
    public GlobalTable global;
    public Stack<Node> ASTStack = new Stack<>();
    public Node currentFunction = null ;
    public Info currentThis = null;
    public FunctionType currentType = null;
    public ClassType currentClass = null;
    public Function tmpFunction;
    public typetable currentTable;
    public Expr trans(Expr now, List<String> nowname, List<Expr>args){
        if(now instanceof Binary){
            System.err.println("enter bina");
            return new Binary(trans(((Binary) now).lhs, nowname, args), ((Binary) now).op, trans(((Binary) now).rhs, nowname, args));
        }else
        if(now instanceof IntConst){
            System.err.println("enter Int");
            return new IntConst(((IntConst) now).value);
        }else{
            System.err.println("enter Expr");
            for(int i = 0; i < nowname.size(); i++)if(((Identifier)now).name.equals(nowname.get(i)))return args.get(i);
        }
        return null;
    }
    public SemanticCheck(GlobalTable global){
       // System.out.println("insemanticcheck");
        this.global = global;
        currentTable = global.globals;
       // System.out.println("outsemanticcheck");
    }
    @Override
    public void visit(ArrayAccess node){
      //  System.out.println("inarrayaccess");
        node.currentScope = currentTable;
        visit(node.array);
        if(node.array.returnType.type!= Type.TheType.ARRAY)throw new RuntimeException("Not an array");
        visit(node.index);
        if(node.index.returnType.type!= Type.TheType.INT)throw new RuntimeException("Invalid index");
        node.isLvalue = true;
        node.returnType = ((ArrayType)node.array.returnType).dataType;
      //  System.out.println("outarrayaccess");
    }
    @Override
    public void visit(Binary node){
        System.err.println("beginBinary");
       // System.out.println("inbinary");
        node.currentScope = currentTable;
        visit(node.lhs);
       // if(node.rhs==null)System.out.print(123);
       // System.out.print(112);
        visit(node.rhs);
        //System.out.print(113);
        System.err.println("checktp");


        if(node.rhs.returnType.type== Type.TheType.INT)System.err.println("rhsint");
        if(node.lhs.returnType.isSame(node.rhs.returnType)){
            Type.TheType nowtype = node.lhs.returnType.type;
            Binary.bop op = node.op;
            if(op == Binary.bop.Plus){
                if(nowtype != Type.TheType.INT && nowtype != Type.TheType.STRING)throw new RuntimeException("Plus needs int or string");
                node.isLvalue = false;
                if(nowtype == Type.TheType.INT)node.returnType = GlobalTable.inttype; else node.returnType = GlobalTable.stringtype;
            }
            if(op == Binary.bop.Module||op == Binary.bop.Times||op == Binary.bop.Minus||op == Binary.bop.Divide||
                    op == Binary.bop.Bxor||op == Binary.bop.Band||op == Binary.bop.Bor||op == Binary.bop.Lshift
                    ||op == Binary.bop.Rshift){
                if(nowtype != Type.TheType.INT)throw new RuntimeException("Operation needs int");
                node.isLvalue = false;
                node.returnType = GlobalTable.inttype;
            }
            if(op == Binary.bop.Greaterthan||op == Binary.bop.GreaterEqualThan||op == Binary.bop.LessThan||
                    op == Binary.bop.LessEqualThan){
                if(nowtype != Type.TheType.INT && nowtype != Type.TheType.STRING)throw new RuntimeException("Compare needs int or string");
                node.isLvalue = false;
                node.returnType = GlobalTable.booltype;
            }
            if(op == Binary.bop.Equal||op == Binary.bop.NotEqual){
                node.isLvalue = false;
                node.returnType = GlobalTable.booltype;
            }
            if(op == Binary.bop.Assign){
                System.err.println("checkassign");
                if(!node.lhs.isLvalue)throw new RuntimeException("Assign needs lvalue");
                node.isLvalue = false;
                node.returnType = node.lhs.returnType;
                if(node.rhs instanceof FunctionCall){
                    FunctionCall fc = (FunctionCall)node.rhs;
                    FunctionType ft = (FunctionType)fc.func.returnType;
                    if(ft.canbeSub){
                        node.rhs = trans(ft.replacement, ft.argumentName, fc.arguments);
                        visit(node.rhs);
                    }
                }
            }
            if(op == Binary.bop.Land||op == Binary.bop.Lor){
                if(nowtype != Type.TheType.BOOL)throw new RuntimeException("Logical operation needs bool");
                node.isLvalue = false;
                node.returnType = node.lhs.returnType;
            }
        }
        else
            throw new RuntimeException("Binary exp needs same type");
       // System.out.println("outbinary");
        if(node.rhs instanceof Binary){
            Binary bnr = (Binary) node.rhs;
            if(bnr.rhs instanceof IntConst && bnr.lhs instanceof IntConst){
                int tmp = ((IntConst) bnr.lhs).value, tmp2 = ((IntConst) bnr.rhs).value;
                if(bnr.op.equals(Binary.bop.Plus)){tmp+=tmp2;node.rhs = new IntConst(tmp);node.rhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Times)){tmp*=tmp2;node.rhs = new IntConst(tmp);node.rhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Divide)){tmp/=tmp2;node.rhs = new IntConst(tmp);node.rhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Minus)){tmp-=tmp2;node.rhs = new IntConst(tmp);node.rhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Lshift)){tmp<<=tmp2;node.rhs = new IntConst(tmp);node.rhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Rshift)){tmp>>=tmp2;node.rhs = new IntConst(tmp);node.rhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Bxor)){tmp^=tmp2;node.rhs = new IntConst(tmp);node.rhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Bor)){tmp|=tmp2;node.rhs = new IntConst(tmp);node.rhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Band)){tmp&=tmp2;node.rhs = new IntConst(tmp);node.rhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Module)){tmp%=tmp2;node.rhs = new IntConst(tmp);node.rhs.returnType = new InitialType(Type.TheType.INT);}
            }
        }
        if(node.lhs instanceof Binary){
            Binary bnr = (Binary) node.lhs;
            if(bnr.rhs instanceof IntConst && bnr.lhs instanceof IntConst){
                int tmp = ((IntConst) bnr.lhs).value, tmp2 = ((IntConst) bnr.rhs).value;
                if(bnr.op.equals(Binary.bop.Plus)){tmp+=tmp2;node.lhs = new IntConst(tmp);
                node.lhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Times)){tmp*=tmp2;node.lhs = new IntConst(tmp);node.lhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Divide)){tmp/=tmp2;node.lhs = new IntConst(tmp);node.lhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Minus)){tmp-=tmp2;node.lhs = new IntConst(tmp);node.lhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Lshift)){tmp<<=tmp2;node.lhs = new IntConst(tmp);node.lhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Rshift)){tmp>>=tmp2;node.lhs = new IntConst(tmp);node.lhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Bxor)){tmp^=tmp2;node.lhs = new IntConst(tmp);node.lhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Bor)){tmp|=tmp2;node.lhs = new IntConst(tmp);node.lhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Band)){tmp&=tmp2;node.lhs = new IntConst(tmp);node.lhs.returnType = new InitialType(Type.TheType.INT);}else
                if(bnr.op.equals(Binary.bop.Module)){tmp%=tmp2;node.lhs = new IntConst(tmp);node.lhs.returnType = new InitialType(Type.TheType.INT);}
            }
        }
        System.err.println("endBinary");
    }
    @Override
    public void visit(Block node){
      //  System.out.println("inblock");
        node.currentScope = currentTable;
        currentTable = new typetable(currentTable);
      //  System.out.print("!!");System.out.println(node.statement.size());
        for(int i = 0; i < node.statement.size(); i++){
            visit(node.statement.get(i));
        }
        currentTable = node.currentScope;
      //  System.out.println("outblock");
    }
    @Override
    public void visit(BoolConst node){
       // System.out.println("inboolconst");
        node.currentScope = currentTable;
        node.returnType = GlobalTable.booltype;
        node.isLvalue = false;
       // System.out.println("outboolconst");
    }
    @Override
    public void visit(Break node){
       // System.out.println("inbreak");
        node.currentScope = currentTable;
        if(ASTStack.empty())throw new RuntimeException("Only break in loop");
       // System.out.println("outbreak");
    }
    @Override
    public void visit(ClassDeclare node){
      //  System.out.println("inclassdeclare");
        node.currentScope = global.globals;
        currentClass = (ClassType) global.globals.map.get(node.name).type;
        currentTable = new typetable(global.globals);
        for(String now : currentClass.member.map.keySet()){
            currentTable.map.put(now, currentClass.member.map.get(now));
        }
        /// /for(VariableDeclare now : node.memberVar)visit(now);
       // System.out.print(1248972414);System.out.println(node.memberFunc.size());
        for(FunctionDeclare now : node.memberFunc)visit(now);
        currentClass = null;
        currentTable = global.globals;
      //  System.out.println("outclassdeclare");
    }
    @Override
    public void visit(Continue node){
      //  System.out.println("incontinue");
        node.currentScope = currentTable;
        if(ASTStack.empty())throw new RuntimeException("Only continue in loop");
      //  System.out.println("outcontinue");
    }
    @Override
    public void visit(Declare node){
       // System.out.println("indeclare");
        node.accept(this);
       // System.out.println("outdeclare");
    }
    @Override
    public void visit(Empty node){
      //  System.out.println("inempty");
        node.currentScope = currentTable;
        node.returnType = GlobalTable.voidtype;
        node.isLvalue = false;
      //  System.out.println("outempty");
    }
    @Override
    public void visit(Expr node){
      //  System.out.println("inexpr");
      //  System.out.println("7");
        node.accept(this);
      //  System.out.println("8");
       // System.out.println("outexpr");
    }
    @Override
    public void visit(For node){
      //  System.out.println("infor");
        node.currentScope = currentTable;
        currentTable = new typetable(currentTable);
        if(node.init!=null)visit(node.init);
        if(node.cond!=null){visit(node.cond);
        if(node.cond.returnType.type!= Type.TheType.BOOL)throw new RuntimeException("Wrong cond for forloop");
        }
        if(node.step!=null)visit(node.step);
        ASTStack.push(node);
        visit(node.content);
        ASTStack.pop();
        currentTable = currentTable.heritage;
      //  System.out.println("outfor");
    }
    @Override
    public void visit(FunctionCall node){
        System.err.println("inFunctioncall");
       // System.out.println("infunctioncall");
        node.currentScope = currentTable;
        visit(node.func);
       if(node.func instanceof Identifier && ((Identifier)node.func).classSub!=null)node.func = ((Identifier)node.func).classSub;
      //  System.out.println(857);
        if(node.func.returnType.type!= Type.TheType.FUNCTION)throw new RuntimeException("Not a function");
        FunctionType nowtype = (FunctionType) node.func.returnType;
      //  System.out.println(nowtype.name);
       // System.out.println(nowtype.argumentType.size());
       // System.out.println(node.arguments.size());
        int ifbuiltin = (GlobalTable.builtinStringFunc.containsValue(nowtype)||GlobalTable.builtinArrayFunc.containsValue(nowtype))? 1 : 0;
      //  System.out.println(ifbuiltin);
        if(node.func instanceof MemberAccess)ifbuiltin = 1;
        System.err.println(123456);
        System.err.println(nowtype.argumentType.size());
        System.err.println(node.arguments.size());
        System.err.println(nowtype.name);

        if(nowtype.argumentType.size()!=ifbuiltin + node.arguments.size())throw new RuntimeException("Wrong argument list");
      //  System.out.println(983);
        System.err.println(65432);
        for (int i = ifbuiltin; i < nowtype.argumentType.size(); i++){
     //       System.out.println(281);
            visit(node.arguments.get(i-ifbuiltin));
      //      System.out.println(123893);
            if(!nowtype.argumentType.get(i).isSame(node.arguments.get(i-ifbuiltin).returnType))throw new RuntimeException("Wrong argument list");
        }
      //  System.out.println(123055);
        if(ifbuiltin > 0) {
             node.perse = ((MemberAccess) node.func).boss;
        }
        node.returnType = nowtype.returnType;
        node.isLvalue = false;
      //  System.out.println("outfunctioncall");
        System.err.println("outfunctioncall");
    }
    @Override
    public void visit(FunctionDeclare node){
        System.err.println("beginFunctionDeclare");
        System.err.println("nowa");
        if(node.iscons == true){
            if(currentClass == null)throw new RuntimeException("Wrong construct func");
            System.err.println("aas");
            System.err.println(node.name);
            if(!node.name.equals(currentClass.name+"_"+currentClass.name))throw new RuntimeException("Wrong construct func");
        }
      //  System.out.println("infunctiondeclare");
      //  System.out.println(node.name);
        System.err.println(11101);
        node.currentScope = currentTable;
        System.err.println(1110);
        currentTable = new typetable(global.globals);
        currentFunction = node;
      //  System.out.print(node.arguments.size());System.out.println("***********");
        if(node.name == null)System.err.println("nn");
        System.err.println(node.name);
        currentType = (FunctionType) currentTable.getType(node.name);
        System.err.println("011");
        if(node.parentClass != null)currentTable.map.put("this", new Info(node.parentClass, 0));
        for(VariableDeclare now : node.arguments){
       //     System.out.println(now.name);
            System.err.println("variable:");
            System.err.println(now.name);
            currentTable.map.put(now.name, new Info(global.getType(now.variableType), currentTable.offset));
            currentTable.offset += 8;
         //   if(currentTable.getType(now.name)==null)System.out.print(101);
            now.currentScope = currentTable;
        }
        currentThis = currentTable.getInfo("this");
        System.err.println("012");
        if(node.parentClass != null)currentThis = currentTable.map.get("this");
      //  if(global.globals.map.get("ptr")!=null)System.out.println(98981);
        for(int i = 0; i < node.content.statement.size(); i++)visit(node.content.statement.get(i));
        currentFunction = null;
        currentThis = null;
        currentType = null;
        currentTable = node.currentScope;
     //   System.out.println("outfunctiondeclare");
        System.err.println("endFunctionDeclare");
    }
    @Override
    public void visit(Identifier node){
        System.err.println("begin Identifier");
      //  System.out.println("iniden");
        node.currentScope = currentTable;
       // System.out.println(node.name);
        if(node.name.equals("this")){
            if(currentClass==null)throw new RuntimeException("Not defined this");
        }
        //if(currentTable.getType(node.name)==null)throw new RuntimeException("Cannot resolve");
        System.err.println(node.name);
        Info nowinfo = currentTable.getInfo(node.name);
        if(nowinfo == null)System.err.println("ckfl");
        if(currentThis != null)
           if(nowinfo == null ||
                   ( ((ClassType)currentTable.getType("this")).member.getInfo(node.name)!=null
                   &&
                           nowinfo != null && nowinfo == global.globals.getInfo(node.name)))
               {

               Identifier newId = new Identifier("this");
               newId.info = currentTable.getInfo("this");
               newId.returnType = newId.info.type;
               MemberAccess ma = new MemberAccess(newId, node.name);
               node.classSub = ma;
               visit(node.classSub);
               node.returnType = node.classSub.returnType;
               node.isLvalue = node.classSub.isLvalue;
               System.err.println("endIdentifier");
               return;
           }

            //  System.out.println(77);
            //   if(nowtype == null)System.out.println(1234);
            if (nowinfo == null || nowinfo.type.type == Type.TheType.VOID)
                throw new RuntimeException("Undefined Identifier");
            //  System.out.println(79);
            node.info = nowinfo;
            node.returnType = nowinfo.type;
            node.isLvalue = (nowinfo.type.type != Type.TheType.FUNCTION);
            //  System.out.println("outiden");

        System.err.println("endIdentifier");
    }
    @Override
    public void visit(If node){
     //   System.out.println("inif");
        node.currentScope = currentTable;
        visit(node.cond);
        if(node.cond.returnType.type!= Type.TheType.BOOL)throw new RuntimeException("Invalid condition");
        currentTable = new typetable(currentTable);
        visit(node.st1);
        currentTable = currentTable.heritage;
        if(node.st2!=null){
            currentTable = new typetable(currentTable);
            visit(node.st2);
            currentTable = currentTable.heritage;
        }
      //  System.out.println("outif");
    }
    @Override
    public void visit(IntConst node){
      //  System.out.println("inintconst");
        node.currentScope = currentTable;
        node.returnType = GlobalTable.inttype;
        node.isLvalue = false;
      //  System.out.println("outintconst");
    }
    @Override
    public void visit(MemberAccess node){
        System.err.println("beginMemberAccess");
      //  System.out.println("inmemberaccess");
      //  System.out.println(node.name);
        node.currentScope = currentTable;
        visit(node.boss);

        Type.TheType nowtype = node.boss.returnType.type;
        //if(node.boss.returnType instanceof  ClassType)System.err.println("eww");else System.err.println("opw");
        if(nowtype == Type.TheType.ARRAY){
        //    System.out.println(1235);
            if(global.builtinArrayFunc.get(node.name)==null)throw new RuntimeException("No such array method");
            FunctionType tmp = global.builtinArrayFunc.get(node.name);
            node.returnType = tmp;
            node.isLvalue = false;
        }

        if(nowtype == Type.TheType.STRING){
        //    System.out.println(1238);
        //    System.out.println(node.name);
            if(global.builtinStringFunc.get(node.name)==null)throw new RuntimeException("No such string method");
            FunctionType tmp = global.builtinStringFunc.get(node.name);
            node.returnType = tmp;
            node.isLvalue = false;
        }
       // currentTable.map.keySet().stream().forEachOrdered(x->System.out.println(x));
        if(nowtype == Type.TheType.CLASS){
            System.err.println("proclass");
          //  System.out.println(2813);
            ClassType nowclass = (ClassType)node.boss.returnType;
         //   System.out.println(2134);
           // ((ClassType)currentTable.getType(nowclass.name)).member.map.keySet().stream().forEachOrdered(
            //     x ->  {System.out.println(x);});
          //  if(global.globals.getType(nowclass.name) instanceof ClassType)System.out.println(213);

            //System.out.println(((ClassType)currentTable.getType(nowclass.name)).name);

          //  System.out.print(12442);
            //if(currentTable.getType(nowclass.name)!=null)System.out.println(123);
            Type rettype;
            if(global.globals.getType(nowclass.name+"_"+node.name)!=null)
            {
                node.name = nowclass.name + "_" + node.name;
                rettype = global.globals.getType(node.name);
                if(rettype.type == Type.TheType.FUNCTION)System.err.println(55);
                if(global.globals.getType(node.name) == null)throw new RuntimeException("No sunc member func");
            }
            else {
                rettype = nowclass.member.getType(node.name);
                if(nowclass.member.getType(node.name)==null)
                throw new RuntimeException("No such member");
            }
            System.err.println("canret");
            node.returnType = rettype;
            if(rettype instanceof FunctionType)node.isLvalue = (((FunctionType)rettype).type == Type.TheType.CLASS);
            else node.isLvalue = true;
        }
        System.err.println("endMemberAccess");
       // System.out.println("outmemberaccess");
    }
    @Override
    public void visit(NewProcess node){
       // System.out.println("innewprocess");
        node.currentScope = currentTable;

        Type nowtype = global.getType(node.type);
        for(int i = 0; i < node.dimension.size(); i++){
         //   System.out.println(1209);
            if(node.dimension.get(i)!=null)
            {Expr now = node.dimension.get(i);
            visit(now);
         //   System.out.println(8);
            if(now.returnType.type != Type.TheType.INT)throw new RuntimeException("Incorrect Index");
            }
            nowtype = new ArrayType(nowtype);
        }
        node.returnType = nowtype;
        node.isLvalue = false;
      //  System.out.println("outnewprocess");
    }
    @Override
    public void visit(Node node){
      //  System.out.println("innode");
        node.accept(this);
     //   System.out.println("outnode");
    }
    @Override
    public void visit(NullConst node){
      //  System.out.println("innullconst");
        node.currentScope = currentTable;
        node.returnType = GlobalTable.nulltype;
        node.isLvalue = false;
      //  System.out.println("outnullconst");
    }
    @Override
    public void visit(Program node){
     //   System.out.println("inprogram");
        node.currentScope = global.globals;
        boolean ck=false;
        for(Declare now: node.declare){now.accept(this);
        if(now instanceof FunctionDeclare)if(((FunctionDeclare) now).name.equals("main"))ck=true;
        }
        if(ck==false)throw new RuntimeException("no main");
      //  System.out.println("outprogram");
    }
    @Override
    public void visit(Return node){
      //  System.out.println("inreturn");
        node.currentScope = currentTable;
        if (currentFunction == null)throw new RuntimeException("Return stmt needs function");
        Type returnType;
     //   System.out.println("eroet");
        if(node.value == null)returnType = GlobalTable.voidtype; else{
            visit(node.value);
            returnType = node.value.returnType;
        }

     //   System.out.println("213");
     //   if(returnType.type== Type.TheType.INT)System.out.println("1923");
        if(currentFunction instanceof FunctionDeclare){
            if( ((FunctionDeclare)currentFunction).iscons==true)if(returnType.type== Type.TheType.VOID
                    ||(node.value instanceof Identifier && ((Identifier)node.value).name.equals("this")))return;
            else throw new RuntimeException("Cannot return anything");
        }
        if(!currentType.returnType.isSame(returnType))throw new RuntimeException("return wrongtype");
      //  System.out.println("outreturn");
    }
    @Override
    public void visit(SelfMinus node){
      //  System.out.println("inselfminus");
        node.currentScope = currentTable;
        visit(node.perse);
        if(!node.perse.isLvalue)throw new RuntimeException("Self dec needs lvalue");
        if(node.perse.returnType.type != Type.TheType.INT)throw new RuntimeException("Self dec needs int");
        node.isLvalue = false;
        node.returnType = GlobalTable.inttype;
      //  System.out.println("outselfminus");
    }
    @Override
    public void visit(SelfPlus node){
       // System.out.println("inselfplus");
        node.currentScope = currentTable;
        visit(node.perse);
     //   System.out.println("2391");
        if(!node.perse.isLvalue)throw new RuntimeException("Self inc needs lvalue");
      //  System.out.println("12355");
        if(node.perse.returnType.type != Type.TheType.INT)throw new RuntimeException("Self inc needs int");
        node.isLvalue = false;
        node.returnType = GlobalTable.inttype;
      //  System.out.println("outselfplus");
    }
    @Override
    public void visit(Stmt node){
     //   System.out.println("instmt");
        node.accept(this);
     //   System.out.println("outstmt");
    }
    @Override
    public void visit(StringConst node){
    //    System.out.println("instringconst");
        node.currentScope = currentTable;
        node.returnType = GlobalTable.stringtype;
        node.isLvalue = false;
      //  System.out.println("outstringconst");
    }
    @Override
    public void visit(Unary node){
       // System.out.println("inunary");
        node.currentScope = currentTable;
        visit(node.perse);
        if(node.op == Unary.uop.SelfP || node.op == Unary.uop.SelfM){
            if(!node.perse.isLvalue)throw new RuntimeException("Self inc or dec needs lvalue!");
            if(node.perse.returnType.type!=Type.TheType.INT)throw new RuntimeException("Self inc or dec needs int!");
            node.returnType = GlobalTable.inttype;
            node.isLvalue = true;
        }
        if(node.op == Unary.uop.Oppo||node.op == Unary.uop.Bnot){
            if(node.perse.returnType.type!=Type.TheType.INT)throw new RuntimeException("Oppo or Binary not needs int!");
            node.returnType = GlobalTable.inttype;
            node.isLvalue = false;
        }
        if(node.op == Unary.uop.Lnot){
            if(node.perse.returnType.type!=Type.TheType.BOOL)throw new RuntimeException("Logical not needs bool");
            node.returnType =GlobalTable.booltype;
            node.isLvalue =false;
        }
     //   System.out.println("outunary");
    }
    @Override
    public void visit(VariableDeclare node){
        if(node.name.equals("this"))throw new RuntimeException("No define this");
     //   System.out.println("invariabledeclare");
        node.currentScope = currentTable;
        System.err.println(node.name);
        if(node.currentScope.getTypeCurrent(node.name)!=null && !node.name.equals("size"))
            throw new RuntimeException("You have declared it in this section");
     //   System.out.println(35);
        VariableType nowtype = global.getType(node.variableType);
      //  System.out.println(36);
        if(nowtype==null)throw new RuntimeException("Cannot resolve");
        if(nowtype.type== Type.TheType.VOID)throw new RuntimeException("Cannot define void");
      //  System.out.println(37);
        if(node.init!=null){
     //       System.out.println(123);
            visit(node.init);
      //      System.out.println(42);
            if(!nowtype.isSame(node.init.returnType))throw new RuntimeException("Cannot init");
      //      System.out.println(43);
        }
      //  System.out.println(2);
        currentTable.map.put(node.name, new Info(nowtype, currentTable.offset));
        currentTable.offset += 8;
       // System.out.println("outvariabledeclare");
    }
    @Override
    public void visit(VariableDeclareStmt node){
      //  System.out.println("invariabledeclarestmt");
        node.currentScope = currentTable;
        visit(node.dec);
      //  System.out.println("outvariabledeclarestmt");
    }
    @Override
    public void visit(While node){
      //  System.out.println("inwhile");
        node.currentScope = currentTable;
        currentTable = new typetable(currentTable);
        if(node.cond==null)throw new RuntimeException("where is your cond?");else {
     //       System.out.println(1275);
            visit(node.cond);
     //       System.out.println(9712);
            if (node.cond.returnType.type != Type.TheType.BOOL) throw new RuntimeException("Wrong cond for forloop");
        }
     //   System.out.println(12984);
        ASTStack.push(node);
        visit(node.content);
        ASTStack.pop();
        currentTable = currentTable.heritage;
      //  System.out.println("outwhile");
    }
    @Override
    public void visit(TypeNode node){

    }
    @Override
    public void visit(InitialTypeNode node){

    }
    @Override
    public void visit(ArrayTypeNode node){

    }
    @Override
    public void visit(FunctionTypeNode node){

    }
    @Override
    public void visit(ClassTypeNode node){

    }
}
