// Generated from /Users/yepro12/compiler2017/src/com/yepro12/compiler2017/Parser/mx.g4 by ANTLR 4.6
package com.yepro12.compiler2017.Parser;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class mxLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.6", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, SELFPLUS=7, SELFMINUS=8, 
		PLUS=9, MINUS=10, TIMES=11, DIVIDE=12, MODULE=13, LSHIFT=14, RSHIFT=15, 
		LESSEQUALTHAN=16, GREATEREQUALTHAN=17, LESSTHAN=18, GREATERTHAN=19, EQUAL=20, 
		NOTEQUAL=21, LAND=22, BAND=23, LOR=24, BOR=25, LNOT=26, BNOT=27, BXOR=28, 
		PIES=29, BOOL=30, INT=31, STRING=32, NULL=33, VOID=34, TRUE=35, FALSE=36, 
		IF=37, ELSE=38, FOR=39, WHILE=40, BREAK=41, CONTINUE=42, RETURN=43, NEW=44, 
		CLASS=45, SEMICOLON=46, COMMA=47, ASSIGN=48, NUM=49, ID=50, CHAIN=51, 
		DIGIT=52, ALPHA=53, COMMENT=54, NEWLINE=55, WS=56, CHAR=57;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "SELFPLUS", "SELFMINUS", 
		"PLUS", "MINUS", "TIMES", "DIVIDE", "MODULE", "LSHIFT", "RSHIFT", "LESSEQUALTHAN", 
		"GREATEREQUALTHAN", "LESSTHAN", "GREATERTHAN", "EQUAL", "NOTEQUAL", "LAND", 
		"BAND", "LOR", "BOR", "LNOT", "BNOT", "BXOR", "PIES", "BOOL", "INT", "STRING", 
		"NULL", "VOID", "TRUE", "FALSE", "IF", "ELSE", "FOR", "WHILE", "BREAK", 
		"CONTINUE", "RETURN", "NEW", "CLASS", "SEMICOLON", "COMMA", "ASSIGN", 
		"NUM", "ID", "CHAIN", "DIGIT", "ALPHA", "COMMENT", "NEWLINE", "WS", "CHAR"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'{'", "'}'", "'('", "')'", "'['", "']'", "'++'", "'--'", "'+'", 
		"'-'", "'*'", "'/'", "'%'", "'<<'", "'>>'", "'<='", "'>='", "'<'", "'>'", 
		"'=='", "'!='", "'&&'", "'&'", "'||'", "'|'", "'!'", "'~'", "'^'", "'.'", 
		"'bool'", "'int'", "'string'", "'null'", "'void'", "'true'", "'false'", 
		"'if'", "'else'", "'for'", "'while'", "'break'", "'continue'", "'return'", 
		"'new'", "'class'", "';'", "','", "'='"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, "SELFPLUS", "SELFMINUS", "PLUS", 
		"MINUS", "TIMES", "DIVIDE", "MODULE", "LSHIFT", "RSHIFT", "LESSEQUALTHAN", 
		"GREATEREQUALTHAN", "LESSTHAN", "GREATERTHAN", "EQUAL", "NOTEQUAL", "LAND", 
		"BAND", "LOR", "BOR", "LNOT", "BNOT", "BXOR", "PIES", "BOOL", "INT", "STRING", 
		"NULL", "VOID", "TRUE", "FALSE", "IF", "ELSE", "FOR", "WHILE", "BREAK", 
		"CONTINUE", "RETURN", "NEW", "CLASS", "SEMICOLON", "COMMA", "ASSIGN", 
		"NUM", "ID", "CHAIN", "DIGIT", "ALPHA", "COMMENT", "NEWLINE", "WS", "CHAR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public mxLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "mx.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2;\u014e\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\3\2\3\2\3\3\3\3\3"+
		"\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\13\3"+
		"\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3\20\3\21\3\21"+
		"\3\21\3\22\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\25\3\26\3\26\3\26"+
		"\3\27\3\27\3\27\3\30\3\30\3\31\3\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34"+
		"\3\35\3\35\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3!\3!\3!\3!"+
		"\3!\3!\3!\3\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3%\3%\3%\3"+
		"%\3%\3%\3&\3&\3&\3\'\3\'\3\'\3\'\3\'\3(\3(\3(\3(\3)\3)\3)\3)\3)\3)\3*"+
		"\3*\3*\3*\3*\3*\3+\3+\3+\3+\3+\3+\3+\3+\3+\3,\3,\3,\3,\3,\3,\3,\3-\3-"+
		"\3-\3-\3.\3.\3.\3.\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\62\6\62\u0118\n\62"+
		"\r\62\16\62\u0119\3\63\3\63\3\63\3\63\7\63\u0120\n\63\f\63\16\63\u0123"+
		"\13\63\3\64\3\64\7\64\u0127\n\64\f\64\16\64\u012a\13\64\3\64\3\64\3\65"+
		"\3\65\3\66\3\66\3\67\3\67\3\67\3\67\7\67\u0136\n\67\f\67\16\67\u0139\13"+
		"\67\3\67\3\67\3\67\3\67\38\58\u0140\n8\38\38\38\38\39\39\39\39\3:\3:\3"+
		":\5:\u014d\n:\3\u0137\2;\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f"+
		"\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63"+
		"\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62"+
		"c\63e\64g\65i\66k\67m8o9q:s;\3\2\7\3\2\62;\4\2C\\c|\5\2\13\f\17\17\"\""+
		"\5\2$$^^pp\4\2$$^^\u0155\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2"+
		"\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25"+
		"\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2"+
		"\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2"+
		"\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3"+
		"\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2"+
		"\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2"+
		"Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3"+
		"\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2"+
		"\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\3u\3\2\2\2\5"+
		"w\3\2\2\2\7y\3\2\2\2\t{\3\2\2\2\13}\3\2\2\2\r\177\3\2\2\2\17\u0081\3\2"+
		"\2\2\21\u0084\3\2\2\2\23\u0087\3\2\2\2\25\u0089\3\2\2\2\27\u008b\3\2\2"+
		"\2\31\u008d\3\2\2\2\33\u008f\3\2\2\2\35\u0091\3\2\2\2\37\u0094\3\2\2\2"+
		"!\u0097\3\2\2\2#\u009a\3\2\2\2%\u009d\3\2\2\2\'\u009f\3\2\2\2)\u00a1\3"+
		"\2\2\2+\u00a4\3\2\2\2-\u00a7\3\2\2\2/\u00aa\3\2\2\2\61\u00ac\3\2\2\2\63"+
		"\u00af\3\2\2\2\65\u00b1\3\2\2\2\67\u00b3\3\2\2\29\u00b5\3\2\2\2;\u00b7"+
		"\3\2\2\2=\u00b9\3\2\2\2?\u00be\3\2\2\2A\u00c2\3\2\2\2C\u00c9\3\2\2\2E"+
		"\u00ce\3\2\2\2G\u00d3\3\2\2\2I\u00d8\3\2\2\2K\u00de\3\2\2\2M\u00e1\3\2"+
		"\2\2O\u00e6\3\2\2\2Q\u00ea\3\2\2\2S\u00f0\3\2\2\2U\u00f6\3\2\2\2W\u00ff"+
		"\3\2\2\2Y\u0106\3\2\2\2[\u010a\3\2\2\2]\u0110\3\2\2\2_\u0112\3\2\2\2a"+
		"\u0114\3\2\2\2c\u0117\3\2\2\2e\u011b\3\2\2\2g\u0124\3\2\2\2i\u012d\3\2"+
		"\2\2k\u012f\3\2\2\2m\u0131\3\2\2\2o\u013f\3\2\2\2q\u0145\3\2\2\2s\u014c"+
		"\3\2\2\2uv\7}\2\2v\4\3\2\2\2wx\7\177\2\2x\6\3\2\2\2yz\7*\2\2z\b\3\2\2"+
		"\2{|\7+\2\2|\n\3\2\2\2}~\7]\2\2~\f\3\2\2\2\177\u0080\7_\2\2\u0080\16\3"+
		"\2\2\2\u0081\u0082\7-\2\2\u0082\u0083\7-\2\2\u0083\20\3\2\2\2\u0084\u0085"+
		"\7/\2\2\u0085\u0086\7/\2\2\u0086\22\3\2\2\2\u0087\u0088\7-\2\2\u0088\24"+
		"\3\2\2\2\u0089\u008a\7/\2\2\u008a\26\3\2\2\2\u008b\u008c\7,\2\2\u008c"+
		"\30\3\2\2\2\u008d\u008e\7\61\2\2\u008e\32\3\2\2\2\u008f\u0090\7\'\2\2"+
		"\u0090\34\3\2\2\2\u0091\u0092\7>\2\2\u0092\u0093\7>\2\2\u0093\36\3\2\2"+
		"\2\u0094\u0095\7@\2\2\u0095\u0096\7@\2\2\u0096 \3\2\2\2\u0097\u0098\7"+
		">\2\2\u0098\u0099\7?\2\2\u0099\"\3\2\2\2\u009a\u009b\7@\2\2\u009b\u009c"+
		"\7?\2\2\u009c$\3\2\2\2\u009d\u009e\7>\2\2\u009e&\3\2\2\2\u009f\u00a0\7"+
		"@\2\2\u00a0(\3\2\2\2\u00a1\u00a2\7?\2\2\u00a2\u00a3\7?\2\2\u00a3*\3\2"+
		"\2\2\u00a4\u00a5\7#\2\2\u00a5\u00a6\7?\2\2\u00a6,\3\2\2\2\u00a7\u00a8"+
		"\7(\2\2\u00a8\u00a9\7(\2\2\u00a9.\3\2\2\2\u00aa\u00ab\7(\2\2\u00ab\60"+
		"\3\2\2\2\u00ac\u00ad\7~\2\2\u00ad\u00ae\7~\2\2\u00ae\62\3\2\2\2\u00af"+
		"\u00b0\7~\2\2\u00b0\64\3\2\2\2\u00b1\u00b2\7#\2\2\u00b2\66\3\2\2\2\u00b3"+
		"\u00b4\7\u0080\2\2\u00b48\3\2\2\2\u00b5\u00b6\7`\2\2\u00b6:\3\2\2\2\u00b7"+
		"\u00b8\7\60\2\2\u00b8<\3\2\2\2\u00b9\u00ba\7d\2\2\u00ba\u00bb\7q\2\2\u00bb"+
		"\u00bc\7q\2\2\u00bc\u00bd\7n\2\2\u00bd>\3\2\2\2\u00be\u00bf\7k\2\2\u00bf"+
		"\u00c0\7p\2\2\u00c0\u00c1\7v\2\2\u00c1@\3\2\2\2\u00c2\u00c3\7u\2\2\u00c3"+
		"\u00c4\7v\2\2\u00c4\u00c5\7t\2\2\u00c5\u00c6\7k\2\2\u00c6\u00c7\7p\2\2"+
		"\u00c7\u00c8\7i\2\2\u00c8B\3\2\2\2\u00c9\u00ca\7p\2\2\u00ca\u00cb\7w\2"+
		"\2\u00cb\u00cc\7n\2\2\u00cc\u00cd\7n\2\2\u00cdD\3\2\2\2\u00ce\u00cf\7"+
		"x\2\2\u00cf\u00d0\7q\2\2\u00d0\u00d1\7k\2\2\u00d1\u00d2\7f\2\2\u00d2F"+
		"\3\2\2\2\u00d3\u00d4\7v\2\2\u00d4\u00d5\7t\2\2\u00d5\u00d6\7w\2\2\u00d6"+
		"\u00d7\7g\2\2\u00d7H\3\2\2\2\u00d8\u00d9\7h\2\2\u00d9\u00da\7c\2\2\u00da"+
		"\u00db\7n\2\2\u00db\u00dc\7u\2\2\u00dc\u00dd\7g\2\2\u00ddJ\3\2\2\2\u00de"+
		"\u00df\7k\2\2\u00df\u00e0\7h\2\2\u00e0L\3\2\2\2\u00e1\u00e2\7g\2\2\u00e2"+
		"\u00e3\7n\2\2\u00e3\u00e4\7u\2\2\u00e4\u00e5\7g\2\2\u00e5N\3\2\2\2\u00e6"+
		"\u00e7\7h\2\2\u00e7\u00e8\7q\2\2\u00e8\u00e9\7t\2\2\u00e9P\3\2\2\2\u00ea"+
		"\u00eb\7y\2\2\u00eb\u00ec\7j\2\2\u00ec\u00ed\7k\2\2\u00ed\u00ee\7n\2\2"+
		"\u00ee\u00ef\7g\2\2\u00efR\3\2\2\2\u00f0\u00f1\7d\2\2\u00f1\u00f2\7t\2"+
		"\2\u00f2\u00f3\7g\2\2\u00f3\u00f4\7c\2\2\u00f4\u00f5\7m\2\2\u00f5T\3\2"+
		"\2\2\u00f6\u00f7\7e\2\2\u00f7\u00f8\7q\2\2\u00f8\u00f9\7p\2\2\u00f9\u00fa"+
		"\7v\2\2\u00fa\u00fb\7k\2\2\u00fb\u00fc\7p\2\2\u00fc\u00fd\7w\2\2\u00fd"+
		"\u00fe\7g\2\2\u00feV\3\2\2\2\u00ff\u0100\7t\2\2\u0100\u0101\7g\2\2\u0101"+
		"\u0102\7v\2\2\u0102\u0103\7w\2\2\u0103\u0104\7t\2\2\u0104\u0105\7p\2\2"+
		"\u0105X\3\2\2\2\u0106\u0107\7p\2\2\u0107\u0108\7g\2\2\u0108\u0109\7y\2"+
		"\2\u0109Z\3\2\2\2\u010a\u010b\7e\2\2\u010b\u010c\7n\2\2\u010c\u010d\7"+
		"c\2\2\u010d\u010e\7u\2\2\u010e\u010f\7u\2\2\u010f\\\3\2\2\2\u0110\u0111"+
		"\7=\2\2\u0111^\3\2\2\2\u0112\u0113\7.\2\2\u0113`\3\2\2\2\u0114\u0115\7"+
		"?\2\2\u0115b\3\2\2\2\u0116\u0118\5i\65\2\u0117\u0116\3\2\2\2\u0118\u0119"+
		"\3\2\2\2\u0119\u0117\3\2\2\2\u0119\u011a\3\2\2\2\u011ad\3\2\2\2\u011b"+
		"\u0121\5k\66\2\u011c\u0120\5i\65\2\u011d\u0120\5k\66\2\u011e\u0120\7a"+
		"\2\2\u011f\u011c\3\2\2\2\u011f\u011d\3\2\2\2\u011f\u011e\3\2\2\2\u0120"+
		"\u0123\3\2\2\2\u0121\u011f\3\2\2\2\u0121\u0122\3\2\2\2\u0122f\3\2\2\2"+
		"\u0123\u0121\3\2\2\2\u0124\u0128\7$\2\2\u0125\u0127\5s:\2\u0126\u0125"+
		"\3\2\2\2\u0127\u012a\3\2\2\2\u0128\u0126\3\2\2\2\u0128\u0129\3\2\2\2\u0129"+
		"\u012b\3\2\2\2\u012a\u0128\3\2\2\2\u012b\u012c\7$\2\2\u012ch\3\2\2\2\u012d"+
		"\u012e\t\2\2\2\u012ej\3\2\2\2\u012f\u0130\t\3\2\2\u0130l\3\2\2\2\u0131"+
		"\u0132\7\61\2\2\u0132\u0133\7\61\2\2\u0133\u0137\3\2\2\2\u0134\u0136\13"+
		"\2\2\2\u0135\u0134\3\2\2\2\u0136\u0139\3\2\2\2\u0137\u0138\3\2\2\2\u0137"+
		"\u0135\3\2\2\2\u0138\u013a\3\2\2\2\u0139\u0137\3\2\2\2\u013a\u013b\5o"+
		"8\2\u013b\u013c\3\2\2\2\u013c\u013d\b\67\2\2\u013dn\3\2\2\2\u013e\u0140"+
		"\7\17\2\2\u013f\u013e\3\2\2\2\u013f\u0140\3\2\2\2\u0140\u0141\3\2\2\2"+
		"\u0141\u0142\7\f\2\2\u0142\u0143\3\2\2\2\u0143\u0144\b8\2\2\u0144p\3\2"+
		"\2\2\u0145\u0146\t\4\2\2\u0146\u0147\3\2\2\2\u0147\u0148\b9\2\2\u0148"+
		"r\3\2\2\2\u0149\u014a\7^\2\2\u014a\u014d\t\5\2\2\u014b\u014d\n\6\2\2\u014c"+
		"\u0149\3\2\2\2\u014c\u014b\3\2\2\2\u014dt\3\2\2\2\n\2\u0119\u011f\u0121"+
		"\u0128\u0137\u013f\u014c\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}