grammar mx;
code : section* EOF;
section : classdefine #classdef
        | function #funcdef
        | vardefine #glbvardef
        ;
stmt : sta #statement;
sta : block #blk
     | exprstmt #expression
     | loop #lp
     | branch #bran
     | jump #jp
     | vardefine #vardef;
block : '{' stmt* '}' ;
branch : IF '(' expr ')' stmt (ELSE stmt)? ;
jump :  RETURN expr? ';' #return
     | BREAK ';' #break
     | CONTINUE  ';' #continue;
loop : FOR '(' init = expr? ';' cond = expr? ';' step = expr? ')' stmt #for
     | WHILE '(' expr ')' stmt #while;
vardefine : arraytype ID ( '=' expr )? ';' ;
classdefine : CLASS  ID '{' member* '}' ;
member : vardefine #clsvar
       | construct #constr
       | function #clsfunc
       ;
construct : arraytype '('')' block ;
function : arraytype ID '(' paralist? ')' block;
paralist : argument ( COMMA argument )*;
argument : arraytype ID;
arraytype : type #nonarray
          | arraytype '['']' #artp
          ;
type : tp = BOOL | tp = INT | tp = VOID | tp = STRING | tp = ID;
exprstmt : expr? ';';
expr : expr op=( SELFMINUS | SELFPLUS ) #selfalt
	 | expr '(' list? ')' #fccall
	 | expr '[' expr ']' #adac
	 | expr PIES ID #mbac
	 | NEW newtype #newp
	 | <assoc = right> op=(SELFPLUS | SELFMINUS) expr #una
	 | <assoc = right> op=MINUS expr #una
	 | <assoc = right> op=(LNOT |BNOT)expr #una
	 | expr op=(TIMES | DIVIDE | MODULE) expr #bina
	 | expr op=( PLUS | MINUS) expr #bina
	 | expr op=(RSHIFT | LSHIFT) expr #bina
	 | expr op=(EQUAL | NOTEQUAL | GREATERTHAN | LESSTHAN | GREATEREQUALTHAN | LESSEQUALTHAN) expr #bina
	 | expr op=BAND expr #bina
	 | expr op=BOR expr #bina
	 | expr op=BXOR expr #bina
	 | expr op=LAND expr #bina
	 | expr op=LOR expr #bina
	 | <assoc = right> expr op=ASSIGN expr #bina
	 | ID #ID
	 | lite #cst
	 | '(' expr ')' #paren
	 ;
lite : tp = NUM
     | tp = TRUE
     | tp =FALSE
     | tp = NULL
     | tp = CHAIN
     ;

newtype : type('[' dim ']')*'['']'('['expr']')('[' dim ']')* #error
        | type('[' dim ']')+ #newarray
        | type #newnonarray
        | type'('')' #newcons;
dim : expr?;
list : expr ( COMMA expr )*;
SELFPLUS : '++';
SELFMINUS : '--';
PLUS : '+';
MINUS : '-';
TIMES : '*';
DIVIDE : '/';
MODULE : '%';
LSHIFT : '<<';
RSHIFT : '>>';
LESSEQUALTHAN : '<=';
GREATEREQUALTHAN : '>=';
LESSTHAN : '<';
GREATERTHAN : '>';
EQUAL : '==';
NOTEQUAL : '!=';
LAND : '&&';
BAND : '&';
LOR : '||';
BOR : '|';
LNOT : '!';
BNOT : '~';
BXOR : '^';
PIES : '.';
BOOL : 'bool';
INT : 'int';
STRING : 'string';
NULL : 'null';
VOID : 'void';
TRUE : 'true';
FALSE : 'false';
IF : 'if';
ELSE : 'else';
FOR : 'for';
WHILE : 'while';
BREAK : 'break';
CONTINUE : 'continue';
RETURN : 'return';
NEW : 'new';
CLASS : 'class';
SEMICOLON : ';';
COMMA : ',';
ASSIGN : '=';
NUM :  DIGIT+ ;
ID : ALPHA(DIGIT | ALPHA | '_')*;
CHAIN : '"' CHAR* '"';
DIGIT : [0-9];
ALPHA : [a-zA-Z];
COMMENT : '//' .*?  NEWLINE-> skip;
NEWLINE : '\r'? '\n' -> skip;
WS : [ \r\n\t] -> skip;
CHAR : '\\' ["\\n] | ~["\\] ;