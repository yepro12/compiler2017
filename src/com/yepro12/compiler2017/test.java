package com.yepro12.compiler2017;
import com.yepro12.compiler2017.AST.ClassDeclare;
import com.yepro12.compiler2017.AST.Declare;
import com.yepro12.compiler2017.AST.FunctionDeclare;
import com.yepro12.compiler2017.AST.Program;
import com.yepro12.compiler2017.Back.*;
import com.yepro12.compiler2017.Front.*;
import com.yepro12.compiler2017.IR.PhysicalRegister;
import com.yepro12.compiler2017.Parser.*;
import com.yepro12.compiler2017.Table.*;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Collection;
import java.util.concurrent.Delayed;

/**
 * Created by yepro12 on 2017/4/5.
 */

public class test {
    public static void main(String[] argv) {
        try {
            PrintStream out = System.out;
            InputStream is = System.in;
            ANTLRInputStream input = new ANTLRInputStream(is);
            mxLexer lexer = new mxLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            mxParser parser = new mxParser(tokens);
            parser.setErrorHandler(new BailErrorStrategy());
            ParseTree tree = parser.code();
            ParseTreeWalker walker = new ParseTreeWalker();
            parser.setErrorHandler(new BailErrorStrategy());
          //  System.out.println("walk");
            ASTGenerator astBuilder = new ASTGenerator();
           // System.out.println("walktree");
            walker.walk(astBuilder, tree);
           // System.out.println("begin");
            parser.setErrorHandler(new BailErrorStrategy());
            Program program = astBuilder.program;
            parser.setErrorHandler(new BailErrorStrategy());
            GlobalTable sym = new GlobalTable();
            ClassScanner cs = new ClassScanner(sym);
            FunctionScanner fs = new FunctionScanner(sym);
            VariableInitializer vi = new VariableInitializer(sym);
            SemanticCheck sc = new SemanticCheck(sym);
          //  System.out.println("start");
            System.err.println("cs bg");
            program.accept(cs);
            System.err.println("fs bg");
            program.accept(fs);
            System.err.println("vi bg");
            program.accept(vi);
            System.err.println("sc bg");
            program.accept(sc);
            System.err.println("sc end");
            IRgenerator irg = new IRgenerator(sym);
            program.accept(irg);
            irg.root.accept(new IRPrinter(System.err));
            GlobalVariableTransformer gvtf = new GlobalVariableTransformer(irg.root);
            gvtf.run();
           // gvtf.root.accept(new IRPrinter(System.err));
            NaiveAllocator na = new NaiveAllocator(gvtf.root, X86RegisterSet.allRegisters);
            System.err.println("begin Allo");
            na.run();
            System.err.println("begin pri");
            na.root.accept(new IRPrinter(System.err));
            System.err.println("begin Targe");
            TargetIRPrinter tirp = new TargetIRPrinter(na.root);
           // System.err.println("Target Over");
            tirp.run();
           // tirp.root.accept(new IRPrinter(System.err));
            tirp.root.accept(new X86Printer(out));
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }
}
