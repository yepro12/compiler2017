package com.yepro12.compiler2017.AST;
import com.yepro12.compiler2017.IR.Operand;
import com.yepro12.compiler2017.Table.*;
public abstract class Expr extends Stmt{
    public Type returnType;
    public boolean isLvalue = true;
    public Operand acvalue;
    public Operand address;
    public int offset;
    public com.yepro12.compiler2017.IR.Block goTrue, goFalse;
}
