package com.yepro12.compiler2017.AST;

import com.yepro12.compiler2017.Table.Info;

public class Identifier extends Expr{
    public MemberAccess classSub = null;
    public String name;
    public Identifier(String name){
        this.name = name;
    }
    public Info info;
    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
