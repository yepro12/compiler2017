package com.yepro12.compiler2017.AST;
import com.yepro12.compiler2017.Table.*;
import java.util.ArrayList;
import java.util.List;

public class FunctionDeclare extends Declare{
    public FunctionType functype;
    public ClassType parentClass = null;
    public TypeNode returnType;
    public String name;
    public List<VariableDeclare> arguments = new ArrayList<>();
    public Block content;
    public boolean iscons = false;
    public FunctionDeclare(TypeNode returnType, String name, List<VariableDeclare> arguments, Block content){
        this.returnType = returnType;
        this.name = name;
        this.arguments = arguments;
        this.content = content;
        this.iscons = false;
    }
    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
