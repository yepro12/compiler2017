package com.yepro12.compiler2017.IR;

/**
 * Created by yepro12 on 2017/5/23.
 */
public interface IRvisitor {
    void visit(Allocate node);
    void visit(BinaryOperation node);
    void visit(Block node);
    void visit(Call node);
    void visit(CJump node);
    void visit(Comparison node);
    void visit(Function node);
    void visit(IRRoot node);
    void visit(Immediate node);
    void visit(Jump node);
    void visit(Load node);
    void visit(Move node);
    void visit(PhysicalRegister node);
    void visit(Return node);
    void visit(StackSlot node);
    void visit(StaticSpace node);
    void visit(StaticString node);
    void visit(Store node);
    void visit(Syscall node);
    void visit(UnaryOperation node);
    void visit(VirtualRegister node);
    void visit(Push node);
    void visit(Pop node);
}
