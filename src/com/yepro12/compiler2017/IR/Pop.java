package com.yepro12.compiler2017.IR;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/25.
 */
public class Pop extends Instruction {
    public Register now;
    public Pop(Block block, Register now){
        super(block);
        this.now = now;
    }

    @Override
    public Collection<Register> getDefinedRegisters() {
        return Collections.singletonList(now);
    }

    @Override
    public Collection<Register> getUsedRegisters() {
        return Collections.emptyList();
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        assert false;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        if(now instanceof Register)now = regMap.get(now);
    }
}