package com.yepro12.compiler2017.IR;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/20.
 */
public class CJump extends Branch {
    public Operand condition;
    public Block gothen, goelse;
    public CJump(Block block, Operand condition, Block gothen, Block goelse){
        super(block);
        this.condition = condition;
        this.gothen = gothen;
        this.goelse = goelse;
    }

    @Override
    public List<Register> getDefinedRegisters() {
        return Collections.emptyList();
    }

    @Override
    public List<Register> getUsedRegisters() {
        if(condition instanceof Register)return Collections.singletonList((Register) condition);
        return Collections.emptyList();
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        assert false;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        if(condition instanceof Register)condition = regMap.get(condition);
    }
}
