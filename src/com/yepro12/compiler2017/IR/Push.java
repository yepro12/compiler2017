package com.yepro12.compiler2017.IR;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/25.
 */
public class Push extends Instruction {
    public Operand now;
    public boolean isstack = false;
    public int offset = 0;
    public Push(Block block, Operand now){
        super(block);
        this.now = now;
    }

    @Override
    public Collection<Register> getDefinedRegisters() {
        return Collections.emptyList();
    }

    @Override
    public Collection<Register> getUsedRegisters() {
        if(now instanceof Register)return Collections.singletonList((Register)now);
        return Collections.emptyList();
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        assert false;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        if(now instanceof Register)now = regMap.get(now);
    }
}
