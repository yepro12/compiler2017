package com.yepro12.compiler2017.IR;

/**
 * Created by yepro12 on 2017/5/23.
 */
public class StackSlot extends Register {
    public Function function;
    public String name;
    public Operand address;
    public int offset;
    public int offset2 = 0;
    public StackSlot(Function function, String name){
        this.function = function;
        this.name = name;
        function.stack.add(this);
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
}
