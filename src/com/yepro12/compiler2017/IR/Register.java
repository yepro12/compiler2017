package com.yepro12.compiler2017.IR;

/**
 * Created by yepro12 on 2017/5/20.
 */
public abstract class Register extends Operand{
    public abstract void accept(IRvisitor visitor);
}
