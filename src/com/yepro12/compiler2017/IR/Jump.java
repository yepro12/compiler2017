package com.yepro12.compiler2017.IR;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/20.
 */
public class Jump extends Branch {
    public Block target;
    public Jump(Block block, Block target){
        super(block);
        this.target = target;
    }

    @Override
    public List<Register> getUsedRegisters() {
        return Collections.emptyList();
    }
    @Override
    public List<Register> getDefinedRegisters(){
        return Collections.emptyList();
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        assert false;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
    }
}
