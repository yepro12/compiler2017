package com.yepro12.compiler2017.IR;


/**
 * Created by yepro12 on 2017/5/20.
 */
public abstract class Branch extends Instruction{
    public Branch(Block block, Instruction prev, Instruction next){
        super(block, prev, next);
    }
    public Branch(Block block){
        super(block);
    }
}
