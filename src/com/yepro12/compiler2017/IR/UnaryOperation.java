package com.yepro12.compiler2017.IR;

import com.yepro12.compiler2017.AST.Unary;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/21.
 */
public class UnaryOperation extends Instruction {
    public enum UnaryOp{
        Not, Oppo;
    }
    public Register dest;
    public Operand oper;
    public UnaryOp Operator;
    public UnaryOperation(Block block, Register dest, UnaryOp Operator, Operand oper){
        super(block);
        this.dest = dest;
        this.Operator = Operator;
        this.oper = oper;
    }

    @Override
    public Collection<Register> getDefinedRegisters() {
        if(dest instanceof  Register)return Collections.singletonList((Register)dest);
        return Collections.emptyList();
    }

    @Override
    public Collection<Register> getUsedRegisters() {
        if(oper instanceof Register)return Collections.singletonList((Register)oper);
        return Collections.emptyList();
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        dest = newReg;
    }

    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        if(oper instanceof Register)oper = regMap.get(oper);
    }
}