package com.yepro12.compiler2017.IR;
import com.yepro12.compiler2017.Table.FunctionType;

import java.util.*;

/**
 * Created by yepro12 on 2017/5/20.
 */
public class Function {
    public List<VirtualRegister> paralist = new ArrayList<>();
    public boolean ismemberfunc = false;
    public int returnsize;
    public String className;
    public String name;
    public Block beginBlock, endBlock;
    public FunctionType type;
    public List<Block> dfsorder = new ArrayList<>();
    public Set<Block> visit = new HashSet<>();
    public List<Return> returns = new ArrayList<>();
    public List<StackSlot> stack = new ArrayList<>();
    public Set<Function> callees = new HashSet<>();
    public Map<VirtualRegister, StackSlot> argStackSlotMap = new HashMap<>();
    public Set<PhysicalRegister> usedPR = new HashSet<>();
    public Function(FunctionType type){
        this.type = type;
        this.name = type.name;
        this.beginBlock = new Block(this, this.name + ".entry");
        this.returnsize = 8;
    }
    public void dfs(Block block){
        if(visit.contains(block))return;
        visit.add(block);
        dfsorder.add(block);
        for(Block i: block.nextBlock)dfs(i);
    }
    public List<Block> dfsOrder(){
        dfsorder = new ArrayList<>();
        visit = new HashSet<>();
        dfs(beginBlock);
        return dfsorder;
    }

    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
}
