package com.yepro12.compiler2017.IR;

/**
 * Created by yepro12 on 2017/5/23.
 */
public abstract class PhysicalRegister extends Register {
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    public abstract String getName();
    public abstract boolean isCallersave();
    public abstract boolean isCalleesave();
}
