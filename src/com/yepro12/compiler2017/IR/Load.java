package com.yepro12.compiler2017.IR;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/21.
 */
public class Load extends Instruction {
    public Register dest;
    public Operand address;
    public int offset;
    public int size;
    public boolean isStaticData;
    public boolean isLoadAddress;
    public Load(Block block, Register dest, int size, Operand address, int offset) {
        super(block);
        this.dest = dest;
        this.size = size;
        this.address = address;
        this.offset = offset;
        this.isStaticData = false;
    }

    public Load(Block block, Register dest, int size, StaticData address, boolean isLoadAddress) {
        super(block);
        this.dest = dest;
        this.size = size;
        this.address = address;
        this.isLoadAddress = isLoadAddress;
        isStaticData = true;
    }

    @Override
    public List<Register> getDefinedRegisters() {
        if(dest instanceof Register)return Collections.singletonList((Register)dest);
        return Collections.emptyList();
    }

    @Override
    public List<Register> getUsedRegisters() {
        if(address instanceof  Register && !(address instanceof  StackSlot))return Collections.singletonList((Register)address);
        return Collections.emptyList();
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        dest = newReg;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        if(address instanceof Register && !(address instanceof StackSlot))address = regMap.get(address);
    }
}
