package com.yepro12.compiler2017.IR;

/**
 * Created by yepro12 on 2017/5/21.
 */
public class StaticSpace extends StaticData {
    public int size;
    public boolean isAddress = false;
    public StaticSpace(String name, int size){
        super(name);
        this.size = size;
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
}
