package com.yepro12.compiler2017.IR;

import com.yepro12.compiler2017.Table.FunctionType;
import com.yepro12.compiler2017.Table.GlobalTable;
import com.yepro12.compiler2017.Back.*;
import java.util.*;

/**
 * Created by yepro12 on 2017/5/21.
 */
public class IRRoot {
    public boolean naive = false;
    public List<StaticData> dataList = new ArrayList<>();
    public Map<String, Function> functions = new LinkedHashMap<>();
    public Map<String, IRClass> classList = new LinkedHashMap<>();
    public Map<String, StaticString> stringPool = new LinkedHashMap<>();
    public Function builtinPrintString;
    public Function builtinPrintlnString;
    public Function builtinToString;
    public Function builtinGetString;
    public Function builtinGetInt;
    public Function builtinStringConcat;
    public Function builtinStringCompare;
    public Function builtinStringParseInt;
    public Function builtinStringSubString;
    public Function builtinStringLength;
    public Function builtinMalloc;
    public List<Function> builtinFunctions = new ArrayList<>();
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }

    public IRRoot(){
        stringPool.put("%s", new StaticString("%s"));
        stringPool.put("%d", new StaticString("%d"));
        stringPool.put("%seefg", new StaticString("%seefg"));
        stringPool.put("buffer", new StaticString("buffer"));
        builtinPrintString = new Function(GlobalTable.voidprint);
        builtinPrintString.paralist.add(new VirtualRegister("arg" + 1));
        builtinPrintString.usedPR.add(X86RegisterSet.RDI);
        builtinPrintString.paralist.add(new VirtualRegister("arg" + 2));
        builtinPrintString.usedPR.add(X86RegisterSet.RSI);
        builtinFunctions.add(builtinPrintString);


        builtinPrintlnString = new Function(GlobalTable.voidprintln);
        builtinPrintlnString.paralist.add(new VirtualRegister("arg" + 1));
        builtinPrintlnString.usedPR.add(X86RegisterSet.RDI);
        builtinPrintlnString.paralist.add(new VirtualRegister("arg" + 2));
        builtinPrintlnString.usedPR.add(X86RegisterSet.RSI);
        builtinFunctions.add(builtinPrintlnString);

        builtinToString = new Function(GlobalTable.stringtostring);
        builtinToString.paralist.add(new VirtualRegister("arg" + 1));
        builtinToString.usedPR.add(X86RegisterSet.RDI);
        builtinToString.paralist.add(new VirtualRegister("arg" + 2));
        builtinToString.usedPR.add(X86RegisterSet.RSI);
        builtinToString.paralist.add(new VirtualRegister("arg" + 3));
        builtinToString.usedPR.add(X86RegisterSet.RDX);
        builtinFunctions.add(builtinToString);

        builtinGetString = new Function(GlobalTable.stringgetstring);
        builtinGetString.paralist.add(new VirtualRegister("arg" + 1));
        builtinGetString.usedPR.add(X86RegisterSet.RDI);
        builtinGetString.paralist.add(new VirtualRegister("arg" + 2));
        builtinGetString.usedPR.add(X86RegisterSet.RSI);
        builtinFunctions.add(builtinGetString);

        builtinGetInt = new Function(GlobalTable.intgetint);
        builtinGetInt.paralist.add(new VirtualRegister("arg" + 1));
        builtinGetInt.usedPR.add(X86RegisterSet.RDI);
        builtinGetInt.paralist.add(new VirtualRegister("arg" + 2));
        builtinGetInt.usedPR.add(X86RegisterSet.RSI);
        builtinFunctions.add(builtinGetInt);

        builtinStringParseInt = new Function(GlobalTable.stringparseInt);
        builtinStringParseInt.paralist.add(new VirtualRegister("arg" + 1));
        builtinStringParseInt.usedPR.add(X86RegisterSet.RDI);
        builtinStringParseInt.paralist.add(new VirtualRegister("arg" + 2));
        builtinStringParseInt.usedPR.add(X86RegisterSet.RSI);
        builtinStringParseInt.paralist.add(new VirtualRegister("arg" + 3));
        builtinStringParseInt.usedPR.add(X86RegisterSet.RDX);
        builtinFunctions.add(builtinStringParseInt);

        builtinStringSubString = new Function(GlobalTable.stringsubstring);
        builtinStringSubString.paralist.add(new VirtualRegister("arg" + 1));
        builtinStringSubString.usedPR.add(X86RegisterSet.RDI);
        builtinStringSubString.paralist.add(new VirtualRegister("arg" + 2));
        builtinStringSubString.usedPR.add(X86RegisterSet.RSI);
        builtinStringSubString.paralist.add(new VirtualRegister("arg" + 3));
        builtinStringSubString.usedPR.add(X86RegisterSet.RDX);
        builtinFunctions.add(builtinStringSubString);

        builtinStringCompare = new Function(GlobalTable.stringcompare);
        builtinStringCompare.paralist.add(new VirtualRegister("arg" + 1));
        builtinStringCompare.usedPR.add(X86RegisterSet.RDI);
        builtinStringCompare.paralist.add(new VirtualRegister("arg" + 2));
        builtinStringCompare.usedPR.add(X86RegisterSet.RSI);
        builtinFunctions.add(builtinStringCompare);

        builtinStringConcat = new Function(GlobalTable.stringplus);
        builtinStringConcat.paralist.add(new VirtualRegister("arg" + 1));
        builtinStringConcat.usedPR.add(X86RegisterSet.RDI);
        builtinStringConcat.paralist.add(new VirtualRegister("arg" + 2));
        builtinStringConcat.usedPR.add(X86RegisterSet.RSI);
        builtinFunctions.add(builtinStringConcat);

        builtinStringLength = new Function(GlobalTable.stringlength);
        builtinStringLength.paralist.add(new VirtualRegister("arg" + 1));
        builtinStringLength.usedPR.add(X86RegisterSet.RDI);
        builtinFunctions.add(builtinStringLength);

        builtinMalloc = new Function(GlobalTable.mallo);
        builtinMalloc.paralist.add(new VirtualRegister("arg" +1));
        builtinMalloc.usedPR.add(X86RegisterSet.RDI);
        builtinFunctions.add(builtinMalloc);
    }
}
