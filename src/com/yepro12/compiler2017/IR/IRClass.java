package com.yepro12.compiler2017.IR;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/22.
 */
public class IRClass {
    public Map<String, Function> functions = new LinkedHashMap<>();
    public String name;
    public IRClass(String name){
        this.name = name;
    }
}
