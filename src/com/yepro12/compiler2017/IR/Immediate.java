package com.yepro12.compiler2017.IR;

/**
 * Created by yepro12 on 2017/5/21.
 */
public class Immediate extends Operand {
    public int value;
    public Immediate(int value){
        this.value = value;
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
}
