package com.yepro12.compiler2017.IR;

/**
 * Created by yepro12 on 2017/5/21.
 */
public class StaticString extends StaticData{
    public String value;
    public StaticString(String value){
        super("str");
        this.value = value;
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
}
