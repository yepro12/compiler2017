package com.yepro12.compiler2017.IR;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/21.
 */
public class Comparison extends Instruction {
    public enum ComOp{
        LessEqualThan, GreaterEqualThan, LessThan, Greaterthan,
        Equal, NotEqual
    }
    public Register dest;
    public Operand lhs, rhs;
    public ComOp Operator;
    public Comparison(Block block, Register dest, ComOp Operator, Operand lhs, Operand rhs){
        super(block);
        this.dest = dest;
        this.Operator = Operator;
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public List<Register> getUsedRegisters() {
        List<Register> ret = new ArrayList<>();
        if(lhs instanceof Register)ret.add((Register)lhs);
        if(rhs instanceof Register)ret.add((Register)rhs);
        return ret;
    }

    @Override
    public List<Register> getDefinedRegisters() {
        if(dest instanceof Register)return Collections.singletonList((Register)dest);
        return Collections.emptyList();
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        dest = newReg;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        if(lhs instanceof Register)lhs = regMap.get(lhs);
        if(rhs instanceof Register)rhs = regMap.get(rhs);
    }
}
