package com.yepro12.compiler2017.IR;

import java.util.*;

/**
 * Created by yepro12 on 2017/5/20.
 */
public class BinaryOperation extends Instruction {
    public enum BinaryOp{
        Plus, Minus, Times, Divide, Module, Lshift, Rshift,
        And, Or, Xor
    }
    public Register dest;
    public Operand lhs, rhs;
    public BinaryOp Operator;
    public BinaryOperation(Block block, Register dest, BinaryOp Operator, Operand lhs, Operand rhs){
        super(block);
        this.dest = dest;
        this.Operator = Operator;
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public List<Register> getUsedRegisters() {
        List<Register> ret = new ArrayList<>();
        if(lhs instanceof Register)ret.add((Register)lhs);
        if(rhs instanceof Register)ret.add((Register)rhs);
        return ret;
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public List<Register> getDefinedRegisters() {
        if(dest instanceof Register)return Collections.singletonList((Register)dest);
        return Collections.emptyList();
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        dest = newReg;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        if(lhs instanceof Register)lhs = regMap.get(lhs);
        if(rhs instanceof Register)rhs = regMap.get(rhs);
    }
}
