package com.yepro12.compiler2017.IR;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/21.
 */
public class Allocate extends Instruction {
    public Operand alsize;
    public Register dest;
    public Allocate(Block block, Register dest, Operand alsize){
        super(block);
        this.alsize = alsize;
        this.dest = dest;
    }

    @Override
    public List<Register> getDefinedRegisters() {
        if(dest instanceof Register)return Collections.singletonList((Register) dest);
        return Collections.emptyList();
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public List<Register> getUsedRegisters() {
        if(alsize instanceof Register)return Collections.singletonList((Register)alsize);
        return Collections.emptyList();
    }

    @Override
    public void setDefinedRegister(Register newReg) {
        dest = newReg;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        if(alsize instanceof Register)alsize = regMap.get(alsize);
    }
}
