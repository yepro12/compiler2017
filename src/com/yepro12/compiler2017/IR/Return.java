package com.yepro12.compiler2017.IR;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/20.
 */
public class Return extends Branch {
    public Operand ret;
    public Return(Block block, Operand ret){
        super(block);
        this.ret = ret;
    }

    @Override
    public Collection<Register> getUsedRegisters() {
        if(ret instanceof  Register)return Collections.singletonList((Register)ret);
        return Collections.emptyList();
    }

    @Override
    public Collection<Register> getDefinedRegisters() {
        return Collections.emptyList();
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        assert false;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        if(ret instanceof Register)ret = regMap.get(ret);
    }
}
