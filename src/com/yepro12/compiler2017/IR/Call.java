package com.yepro12.compiler2017.IR;
import java.util.*;
/**
 * Created by yepro12 on 2017/5/22.
 */
public class Call extends Instruction {
    public Register dest;
    public Function function;
    public List<Operand> arguments = new ArrayList<>();
    public Call(Block block, Register dest, Function function){
        super(block);
        this.dest = dest;
        this.function = function;
    }

    @Override
    public Collection<Register> getUsedRegisters() {
        List<Register> ret = new ArrayList<>();
        arguments.stream().filter(x -> x instanceof Register).forEach(x -> ret.add((Register) x));
        return ret;
    }

    @Override
    public Collection<Register> getDefinedRegisters() {
        if(dest instanceof Register)return Collections.singletonList((Register) dest);
        return Collections.emptyList();
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        dest = newReg;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        List<Operand> tmp = new ArrayList<>();
        for(int i = 0; i < arguments.size();i++)if(arguments.get(i) instanceof Register)
            tmp.add(regMap.get(arguments.get(i)));else tmp.add(arguments.get(i));
        arguments = tmp;
        for(Operand now:arguments)if(now instanceof StaticData)System.err.println(9213);
    }
}
