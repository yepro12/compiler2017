package com.yepro12.compiler2017.IR;

/**
 * Created by yepro12 on 2017/5/20.
 */
public class VirtualRegister extends Register{
    public String name;
    public PhysicalRegister forcedPhysicalRegister;
    public VirtualRegister(String name){
        this.name = name;
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
}
