package com.yepro12.compiler2017.IR;


import com.sun.org.apache.regexp.internal.RE;

import java.util.*;
import java.util.List;

/**
 * Created by yepro12 on 2017/5/20.
 */
public abstract class Instruction {
    public Set<VirtualRegister> liveIn = new HashSet<>();
    public Set<VirtualRegister> liveOut = new HashSet<>();
    public boolean exist = true;
    public Block currentBlock;
    public Instruction prev = null;
    public Instruction next = null;
    public Instruction(Block currentBlock, Instruction prev,
                       Instruction next){
        this.currentBlock = currentBlock;
        this.prev = prev;
        this.next = next;
    }
    public Instruction(Block currentBlock){
        this.currentBlock = currentBlock;
    }
    public void linknext(Instruction now){
        next = now;
        now.prev = this;
    }
    public void linkprev(Instruction now){
        prev = now;
        now.next = this;
    }
    public void prepend(Instruction now){
        if(prev == null) currentBlock.begin = now; else
            prev.linknext(now);
        now.linknext(this);
    }
    public void append(Instruction now) {
        if (next == null) currentBlock.end = now; else
            next.linkprev(now);
        now.linkprev(this);
    }
    public  void delete(){
        assert exist;
        exist = false;
        if(this instanceof Branch) {
            currentBlock.finished = false;
            if(this instanceof Jump){
                currentBlock.sub(((Jump) this).target);
            }
            if(this instanceof CJump){
                currentBlock.sub(((CJump)this).goelse);
                currentBlock.sub(((CJump)this).gothen);
            }
            if(this instanceof Return)
                currentBlock.currentFunc.returns.remove(this);
        }
        if(prev != null)prev.next = next;
        if(next != null)next.prev = prev;
        if(currentBlock.end == this)currentBlock.end = prev;
        if(currentBlock.begin == this)currentBlock.begin = next;
    }
    public abstract Collection<Register> getDefinedRegisters();
    public abstract Collection<Register> getUsedRegisters();
    public abstract void setDefinedRegister(Register newReg);
    public abstract void setUsedRegister(Map<Register, Register> regMap);
    public abstract void accept(IRvisitor visitor);
}
