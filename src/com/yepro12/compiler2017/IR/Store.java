package com.yepro12.compiler2017.IR;

import java.util.Collection;
import java.util.Collections;
import java.util.*;
/**
 * Created by yepro12 on 2017/5/21.
 */
public class Store extends Instruction {
    public int size;
    public Operand value;
    public Operand address;
    public int offset;
    public boolean isStaticData;

    public Store(Block block, Operand value, int size, Operand address, int offset) {
        super(block);
        this.size = size;
        this.address = address;
        this.offset = offset;
        this.value = value;
        this.isStaticData = false;
    }
    public Store(Block block, Operand value, int size, StaticData address) {
        super(block);
        this.size = size;
        this.address = address;
        this.offset = 0;
        this.value = value;
        this.isStaticData = true;
    }

    @Override
    public Collection<Register> getDefinedRegisters() {
        return Collections.emptyList();
    }

    @Override
    public Collection<Register> getUsedRegisters() {
        List<Register> ret = new ArrayList<>();
        if(value instanceof Register)ret.add((Register)value);
        if(address instanceof Register && !(address instanceof StackSlot))ret.add((Register)address);
        return ret;
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        assert false;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        if(value instanceof Register)value = regMap.get(value);
        if(address instanceof Register && !(address instanceof StackSlot))address = regMap.get(address);
    }
}
