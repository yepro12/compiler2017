package com.yepro12.compiler2017.IR;

/**
 * Created by yepro12 on 2017/5/21.
 */
public abstract class StaticData extends Register {
    public String name;
    public StaticData(String name){
        this.name = name;
    }
    public abstract void accept(IRvisitor visitor);
}
