package com.yepro12.compiler2017.IR;

import java.util.*;
/**
 * Created by yepro12 on 2017/5/20.
 */
public class Block {
    public int ofsss = 0;
    public Instruction begin = null;
    public Instruction end = null;
    public String name;
    public Function currentFunc;
    public boolean finished = false;
    public Set<Block> prevBlock = new HashSet<>();
    public Set<Block> nextBlock = new HashSet<>();
    public Block(Function currentFunc, String name){
        this.currentFunc = currentFunc;
        this.name = name;
    }
    public void finish(Instruction now){
        finished = true;
        append(now);
        assert now instanceof Branch;
        if(now instanceof Jump)nextBlock.add(((Jump)now).target);
        if(now instanceof Return)currentFunc.returns.add((Return) now);
        if(now instanceof CJump){
            CJump tmp = (CJump) now;
            if(tmp.goelse!=null)
            nextBlock.add(tmp.goelse);
            if(tmp.gothen!=null)
            nextBlock.add(tmp.gothen);
        }
    }
    public void append(Instruction now){
        assert !finished;
        if(end == null)begin = end = now; else
        {
            end.linknext(now);
            end = now;
        }
    }
    public void bppend(Instruction now){
        assert finished;
        if(end.prev == null)begin = now; else
            end.prev.linknext(now);
        now.linknext(end);
    }
    public void add(Block block){
        if(block != null){
            nextBlock.add(block);
            block.prevBlock.add(this);
        }
    }
    public void sub(Block block){
        if(block != null){
            nextBlock.remove(block);
            block.prevBlock.remove(block);
        }
    }

    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
}
