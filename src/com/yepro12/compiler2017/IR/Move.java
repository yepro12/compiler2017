package com.yepro12.compiler2017.IR;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/21.
 */
public class Move extends Instruction {
    public Register dest;
    public Operand rhs;
    public Move(Block block, Register dest, Operand rhs){
        super(block);
        this.dest = dest;
        this.rhs = rhs;
    }

    @Override
    public Collection<Register> getDefinedRegisters() {
        if(dest instanceof  Register)return Collections.singletonList(dest);
        return Collections.emptyList();
    }

    @Override
    public Collection<Register> getUsedRegisters() {
        if(rhs instanceof  Register)return Collections.singletonList((Register)rhs);
        return Collections.emptyList();
    }
    @Override
    public void accept(IRvisitor visitor) {
        visitor.visit(this);
    }
    @Override
    public void setDefinedRegister(Register newReg) {
        dest = newReg;
    }
    @Override
    public void setUsedRegister(Map<Register, Register> regMap) {
        if(rhs instanceof Register)rhs = regMap.get(rhs);
    }
}
