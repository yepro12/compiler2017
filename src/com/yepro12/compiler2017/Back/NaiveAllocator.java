package com.yepro12.compiler2017.Back;

import com.yepro12.compiler2017.IR.*;
import java.util.*;

/**
 * Created by yepro12 on 2017/5/23.
 */
public class NaiveAllocator {
    public IRRoot root;
    public Function func;
    public List<PhysicalRegister> regs = new ArrayList<>();
    public Map<VirtualRegister, StackSlot> slots = new HashMap<>();
    public NaiveAllocator(IRRoot root, Collection<PhysicalRegister> regs) {
        this.root = root;
        this.regs.addAll(regs);
    }

    public StackSlot getStackSlot(VirtualRegister vr) {
        StackSlot slot = slots.get(vr);
        if (slot == null) {
            slot = new StackSlot(func, vr.name);
            slots.put(vr, slot);
        }
        return slot;
    }

    public void processFunction() {
        int cc = 0;
        slots.clear();
        slots.putAll(func.argStackSlotMap);
        Map<Register, Register> regRenameMap = new HashMap<>();
        for (Block BB : func.dfsOrder()) {
            for (Instruction inst = BB.begin; inst != null; inst = inst.next) {
                int cnt = 0;
                if (!(inst instanceof Call)) {
                    if(inst instanceof  Return)System.err.println(12312);
                    Collection<Register> used = inst.getUsedRegisters();
                    if (!used.isEmpty()) {
                        regRenameMap.clear();
                        used.forEach(x -> regRenameMap.put(x, x));
                        for (Register reg : used)
                            if (reg instanceof VirtualRegister) {
                                PhysicalRegister pr = ((VirtualRegister) reg).forcedPhysicalRegister;
                                if (pr == null) pr = regs.get(cnt++);
                                regRenameMap.put(reg, pr);
                                func.usedPR.add(pr);
                                Register addr = getStackSlot((VirtualRegister) reg);
                                inst.prepend(new Load(BB, pr, 8, addr, 0));
                            }

                        inst.setUsedRegister(regRenameMap);
                    }
                } else {
                    List<Operand> args = ((Call) inst).arguments;
                    for (int i = 0; i < args.size(); ++i) {
                        Operand val = args.get(i);
                        if (val instanceof VirtualRegister) {
                            Register addr = getStackSlot((VirtualRegister) val);
                            args.set(i, addr);
                        }
                    }
                }
                for(Register defined : inst.getDefinedRegisters())if(defined instanceof VirtualRegister){
                    PhysicalRegister pr = ((VirtualRegister) defined).forcedPhysicalRegister;
                    if (pr == null) pr = regs.get(cnt++);
                    func.usedPR.add(pr);
                    inst.setDefinedRegister(pr);
                    Register addr = getStackSlot((VirtualRegister) defined);
                    inst.append(new Store(BB, pr, 8, addr, 0));
                    inst = inst.next;
                }
            }
        }
    }

    public void run() {
        for (Function function : root.functions.values()) {
            func = function;
            processFunction();
        }
    }
}

