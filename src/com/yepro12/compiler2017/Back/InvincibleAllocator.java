package com.yepro12.compiler2017.Back;

import com.yepro12.compiler2017.IR.*;

import java.util.*;

/**
 * Created by yepro12 on 2017/6/9.
 */
public class InvincibleAllocator {
    public int totalcolor;
    public IRRoot root;
    public Function func;
    public Stack<VirtualRegister> stack = new Stack<>();
    public Set<VirtualRegister> nodes = new HashSet<>();
    public Set<VirtualRegister> goodNodes = new HashSet<>();
    public List<PhysicalRegister> regs;
    public Set<PhysicalRegister> used = new HashSet<>();
    public Map<VirtualRegister, vrinfo> vrInfo = new HashMap<>();
    public Map<Register, Register> renameMap = new HashMap<>();
    public InvincibleAllocator(IRRoot root, Collection<PhysicalRegister> regs) {
        this.root = root;
        this.regs = new ArrayList<>(regs);
        this.totalcolor = this.regs.size();
    }
    public class vrinfo{
        Set<VirtualRegister> neighbour = new HashSet<>();
        Set<VirtualRegister> recommend = new HashSet<>();
        int degree = 0;
        Register usedcolor = null;
        boolean deleted = false;
    }
    public vrinfo getvrinfo(VirtualRegister vr) {
        vrinfo info = vrInfo.get(vr);
        if (info == null) {
            info = new vrinfo();
            vrInfo.put(vr, info);
        }
        return info;
    }
    public void addEdge(VirtualRegister x, VirtualRegister y) {
       // System.err.println("addedg");
        getvrinfo(x).neighbour.add(y);
        getvrinfo(y).neighbour.add(x);
    }
    public void build(){
       // System.err.println("begin build");
        List<VirtualRegister> args = func.paralist;
        args.forEach(this::getvrinfo);
        for(Block block : func.dfsOrder()){
            for(Instruction inst = block.begin; inst != null; inst = inst.next) {
             //   System.err.print("size=");System.err.println(inst.liveOut.size());
                for(Register defined : inst.getDefinedRegisters()) if(defined instanceof VirtualRegister){
                    vrinfo info = getvrinfo((VirtualRegister) defined);
                    if(inst instanceof Move) {
                        Operand rhs = ((Move) inst).rhs;
                        if (rhs instanceof VirtualRegister) {
                            info.recommend.add((VirtualRegister)rhs);
                            getvrinfo((VirtualRegister)rhs).recommend.add((VirtualRegister)defined);
                        }
                        inst.liveOut.stream().filter(x -> x != rhs && x != defined).forEach(x -> addEdge(x, (VirtualRegister) defined));
                    } else {
                        inst.liveOut.stream().filter(x -> x != defined).forEach(x -> addEdge(x, (VirtualRegister) defined));
                    }
                }
            }
        }
        vrInfo.values().forEach(x -> x.degree = x.neighbour.size());
        System.err.println("end build");
    }
    public void removeNode(VirtualRegister node){
        for(VirtualRegister now: vrInfo.get(node).neighbour){
            vrinfo nb = vrInfo.get(now);
            if(!nb.deleted){
                nb.degree -= 1;
                if(nb.degree < totalcolor)goodNodes.add(now);
            }
        }
        vrInfo.get(node).deleted = true;
        nodes.remove(node);
        stack.push(node);
    }
    public void paint(){
        System.err.println("begin paint");
        nodes.addAll(vrInfo.keySet());

        nodes.stream().filter(x-> vrInfo.get(x).neighbour.size() < totalcolor).forEach(goodNodes::add);
        System.err.println("phase 1");
        while(!nodes.isEmpty()){
            while(!goodNodes.isEmpty()){
               // System.err.println(goodNodes.size());
                Iterator<VirtualRegister> it = goodNodes.iterator();
                VirtualRegister now = it.next();
                it.remove();
                removeNode(now);

            }
            if(nodes.isEmpty())break;
            Iterator<VirtualRegister> it = nodes.iterator();
            VirtualRegister now = it.next();
            it.remove();
            removeNode(now);
        }
        System.err.println("phase 2");
        while(!stack.empty()){
            VirtualRegister node = stack.pop();
            vrinfo info = vrInfo.get(node);
            used.clear();
            for(VirtualRegister nb: info.neighbour){
                vrinfo nbinfo = vrInfo.get(nb);
                if(!nbinfo.deleted && nbinfo.usedcolor instanceof PhysicalRegister) used.add((PhysicalRegister) nbinfo.usedcolor);
            }
            PhysicalRegister forced = node.forcedPhysicalRegister;
            if(forced != null) info.usedcolor = forced;
            else
            {
                for(VirtualRegister vr: info.recommend){
                    Register reg = getvrinfo(vr).usedcolor;
                    if(reg instanceof PhysicalRegister && !used.contains(reg)){
                        info.usedcolor = reg;
                        break;
                    }
                }
                if(info.usedcolor == null){
                    for(PhysicalRegister pr : regs){
                        if(!used.contains(pr)) {
                            info.usedcolor = pr;
                            break;
                        }
                    }
                    if(info.usedcolor == null){
                        info.usedcolor = func.argStackSlotMap.get(node);
                        if(info.usedcolor == null) info.usedcolor = new StackSlot(func, node.name);
                    }
                }
            }
            info.deleted = false;
        }
        System.err.println("end paint");
    }
    public void rewrite(){
        System.err.println("begin rewrite");
        for(Block block: func.dfsOrder()){
            for(Instruction inst = block.begin; inst != null; inst = inst.next){
                Collection<Register> used = inst.getUsedRegisters();
                if(!(inst instanceof Call)){
                    if (!used.isEmpty()) {
                        boolean RAXUsed = false;
                        renameMap.clear();
                        for (Register reg : used){
                            if (reg instanceof VirtualRegister){
                                Register color = vrInfo.get(reg).usedcolor;
                                if (color instanceof StackSlot){
                                    PhysicalRegister pr = RAXUsed ? X86RegisterSet.R10 : X86RegisterSet.R11;
                                    inst.prepend(new Load(block, pr, 8, color, 0));
                                    RAXUsed = true;
                                    renameMap.put(reg, pr);
                                    func.usedPR.add(pr);
                                }
                                else
                                {
                                    renameMap.put(reg, color);
                                    func.usedPR.add((PhysicalRegister) color);
                                }
                            }
                            else renameMap.put(reg, reg);
                        }
                        inst.setUsedRegister(renameMap);
                    }
                }
                else
                {
                    List<Operand> args = ((Call) inst).arguments;
                    for (int i = 0; i < args.size(); ++i){
                        Operand val = args.get(i);
                        if (val instanceof VirtualRegister) args.set(i, vrInfo.get(val).usedcolor);
                    }
                }
                for(Register defined: inst.getDefinedRegisters()) if(defined instanceof VirtualRegister){
                    Register color = vrInfo.get(defined).usedcolor;
                    if (color instanceof StackSlot){
                        inst.append(new Store(block, X86RegisterSet.R10, 8, color, 0));
                        inst.setDefinedRegister(X86RegisterSet.R10);
                        func.usedPR.add(X86RegisterSet.R10);
                        inst = inst.next;
                    } else {
                        inst.setDefinedRegister(color);
                        func.usedPR.add((PhysicalRegister) color);
                    }
                }
            }
        }
        System.err.println("end rewrite");
    }
    public void run(){
        new LivelinessAnalyzer(root).run();
        for(Function function : root.functions.values()){
            func = function;
            stack.clear();
            nodes.clear();
            goodNodes.clear();;
            build();
            paint();
            rewrite();
        }
    }
}

