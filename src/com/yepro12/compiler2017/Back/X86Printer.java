package com.yepro12.compiler2017.Back;

import com.yepro12.compiler2017.AST.Binary;
import com.yepro12.compiler2017.IR.*;
import java.io.*;
import java.util.*;
/**
 * Created by yepro12 on 2017/5/24.
 */
public class X86Printer implements IRvisitor {
    public PrintStream out;
    public Map<String, Integer> accumulater = new HashMap<>();
    public Map<Object, String> nameMap = new HashMap<>();
    public Map<Block, Integer> blockNumber = new HashMap<>();
    public Block currentBlock;
    public boolean isDefiningStaticData;

    public X86Printer(PrintStream out) {
        this.out = out;
    }

    public String newId(String name) {
        int cnt = accumulater.getOrDefault(name, 0) + 1;
        accumulater.put(name, cnt);
        return name + "_" + cnt;
    }

    public String blockLabel(Block block) {
        String id = nameMap.get(block);
        if (id == null) {
            id = newId(block.name);
            nameMap.put(block, id);
        }
        if(id.equals("main.entry_1"))id = "main";
        return id;
    }

    public String dataId(StaticData node) {
        String id = nameMap.get(node);
        if (id == null) {
            id = newId(node.name);
            nameMap.put(node, id);
        }
        return id;
    }
    @Override
    public void visit(Push node){
        out.printf("push ");
        node.now.accept(this);
        out.println();
    }
    @Override
    public void visit(Pop node){
        out.printf("pop ");
        node.now.accept(this);
        out.println();
    }
    @Override
    public void visit(IRRoot node) {
        out.println("global main");
        out.println("extern malloc");
        out.println("extern printf");
        out.println("extern sprintf");
        out.println("extern sscanf");
        out.println("extern strncpy");
        out.println("extern strlen");
        out.println("extern strcmp");
        out.println("extern strcpy");
        out.println("extern strcat");
        out.println("extern scanf");
        out.println("section .text");
        out.println("mallo:");
        out.println("push rdi");
        out.println("add rdi, 1");
        out.println("call malloc");
        out.println("pop rdi");
        out.println("ret");
        out.println("strcompare:");
        out.println("push rdi");
        out.println("push rsi");
        out.println("sub rsp, 8");
        out.println("call strcmp");
        out.println("add rsp, 8");
        out.println("pop rsi");
        out.println("pop rdi");
        out.println("ret");
        out.println("printwithenter:");
        out.println("push rdi");
        out.println("mov rsi, rdi");
        out.println("mov rdi, enterformat");
        out.println("call printf");
        out.println("pop rdi");
        out.println("ret");
        out.println("printwithoutenter:");
        out.println("push rdi");
        out.println("mov rsi, rdi");
        out.println("mov rdi, noenterformat");
        out.println("call printf");
        out.println("pop rdi");
        out.println("ret");
        out.println("tostring:");
        out.println("sub rsp, 8");
        out.println("push rdi");
        out.println("push rsi");
        out.println("mov rdx, rsi");
        out.println("mov rsi, formatnumber");
        out.println("mov rdi, stringbuffer");
        out.println("call sprintf");
        out.println("mov rdi, stringbuffer");
        out.println("call strlen");
        out.println("mov rdi, rax");
        out.println("add rdi, 1");
        out.println("call malloc");
        out.println("mov rdi, rax");
        out.println("mov rsi, stringbuffer");
        out.println("call strcpy");
        out.println("pop rsi");
        out.println("pop rdi");
        out.println("add rsp, 8");
        out.println("ret");
        out.println("substr:");
        out.println("push rdi");
        out.println("push rsi");
        out.println("mov rdi, rsi");
        out.println("add rdi, 1");
        out.println("call malloc");
        out.println("mov rdi, rax");
        out.println("pop rdx");
        out.println("pop rsi");
        out.println("push rdi");
        out.println("push rsi");
        out.println("call strncpy");
        out.println("mov rax, rdi");
        out.println("pop rsi");
        out.println("pop rdi");
        out.println("ret");
        out.println("parseint:");
        out.println("push rdi");
        out.println("push rsi");
        out.println("push rdx");
        out.println("mov rsi, formatnumber");
        out.println("mov rdx, numbuffer");
        out.println("call sscanf");
        out.println("mov rax, [numbuffer]");
        out.println("pop rdx");
        out.println("pop rsi");
        out.println("pop rdi");
        out.println("ret");
        out.println("getstr:");
        out.println("push rdi");
        out.println("push rsi");
        out.println("mov rdi, stringformat");
        out.println("mov rsi, stringbuffer");
        out.println("sub rsp, 8");
        out.println("call scanf");
        out.println("mov rdi, stringbuffer");
        out.println("call strlen");
        out.println("mov rdi, rax");
        out.println("add rdi, 1");
        out.println("call malloc");
        out.println("mov rdi, rax");
        out.println("mov rsi, stringbuffer");
        out.println("call strcpy");
        out.println("add rsp, 8");
        out.println("pop rsi");
        out.println("pop rdi");
        out.println("ret");
        out.println("getint:");
        out.println("push rdi");
        out.println("push rsi");
        out.println("mov rdi, formatnumber");
        out.println("mov rsi, numbuffer");
        out.println("sub rsp, 8");
        out.println("call scanf");
        out.println("add rsp, 8");
        out.println("mov rax, [numbuffer]");
        out.println("pop rsi");
        out.println("pop rdi");
        out.println("ret");
        out.println("stringcat:");
        out.println("push rdi");
        out.println("push rsi");
        out.println("mov rsi, rdi");
        out.println("mov rdi, stringbuffer");
        out.println("call strcpy");
        out.println("mov rdi, stringbuffer");
        out.println("pop rsi");
        out.println("call strcat");
        out.println("mov rdi, rax");
        out.println("call strlen");
        out.println("mov rdi, rax");
        out.println("add rdi, 1");
        out.println("call malloc");
        out.println("push rsi");
        out.println("mov rdi, rax");
        out.println("mov rsi, stringbuffer");
        out.println("call strcpy");
        out.println("pop rsi");
        out.println("pop rdi");
        out.println("ret");
        node.functions.values().stream().filter(x -> !(node.builtinFunctions.contains(x))).forEach(this::visit);
        out.println("section .data");
        isDefiningStaticData = true;
        node.stringPool.values().forEach(this::visit);
        node.dataList.forEach(x -> x.accept(this));
        isDefiningStaticData = false;
        out.println("_boolean_const_true:");
        out.println("dq\t 1");
        out.println("formatnumber:");
        out.println("db\t \"%d\",0");
        out.println("numbuffer:");
        out.println("dq 0");
        out.println("stringbuffer:");
        out.println("times 1000 db 0");
        out.println("stringformat:");
        out.println("db\t\"%s\",0");
        out.println("noenterformat:");
        out.println("db\t\"%s\",0");
        out.println("enterformat:");
        out.println("db\t\"%s\",10,0");
    }

    @Override
    public void visit(Block node) {
        out.printf("%s:\n", blockLabel(node));
        if(blockLabel(node).equals("main")){out.println("mov rbp, rsp");out.printf("add rbp, %d\n", node.ofsss);}
        currentBlock = node;
        for (Instruction i = node.begin; i != null; i = i.next) i.accept(this);
    }

    @Override
    public void visit(Function node) {
        List<Block> tmp = node.dfsOrder();
        for (int i = 0; i < tmp.size(); ++i) blockNumber.put(tmp.get(i), i);
        tmp.forEach(this::visit);
    }

    @Override
    public void visit(BinaryOperation node) {
        if(node.dest instanceof PhysicalRegister && node.rhs instanceof PhysicalRegister
                && ((PhysicalRegister)node.dest).getName().equals(((PhysicalRegister)node.rhs).getName())){
            if(node.Operator.equals(BinaryOperation.BinaryOp.Times)||
                    node.Operator.equals(BinaryOperation.BinaryOp.Plus)||
                    node.Operator.equals(BinaryOperation.BinaryOp.And)||
                    node.Operator.equals(BinaryOperation.BinaryOp.Xor)||
                    node.Operator.equals(BinaryOperation.BinaryOp.And)||
                    node.Operator.equals(BinaryOperation.BinaryOp.Or)){
                Operand sub = node.rhs;
                node.rhs = node.lhs;
                node.lhs = sub;
            }
        }
        if(node.lhs instanceof Immediate){

            if(node.Operator.equals(BinaryOperation.BinaryOp.Times)||
                    node.Operator.equals(BinaryOperation.BinaryOp.Plus)||
                    node.Operator.equals(BinaryOperation.BinaryOp.And)||
                    node.Operator.equals(BinaryOperation.BinaryOp.Xor)||
                    node.Operator.equals(BinaryOperation.BinaryOp.And)||
                    node.Operator.equals(BinaryOperation.BinaryOp.Or))
            {
                Operand sub = node.rhs;
                node.rhs = node.lhs;
                node.lhs = sub;
            }
        }
        if(node.rhs instanceof Immediate){

            Immediate im = (Immediate)node.rhs;
            if(im.value == 1){
               // System.err.println("changed1");
                if(node.Operator.equals(BinaryOperation.BinaryOp.Divide) || node.Operator.equals(BinaryOperation.BinaryOp.Times))
                {
                    out.print("mov ");
                    node.dest.accept(this);
                    out.print(", ");
                    node.lhs.accept(this);
                    out.println();
                    return;
                }
            }else
            if(im.value == 0){
                //System.err.println("changed0");
                if(node.Operator.equals(BinaryOperation.BinaryOp.Plus)||
                        node.Operator.equals(BinaryOperation.BinaryOp.Minus)||
                        node.Operator.equals(BinaryOperation.BinaryOp.Lshift)||
                        node.Operator.equals(BinaryOperation.BinaryOp.Rshift)){
                    out.print("mov ");
                    node.dest.accept(this);
                    out.print(", ");
                    node.lhs.accept(this);
                    out.println();
                    return;
                }
            }else
            if(im.value == 2){
                //System.err.println("changed2");
                if(node.Operator.equals(BinaryOperation.BinaryOp.Divide)){
                    node.Operator = BinaryOperation.BinaryOp.Rshift;
                    im.value = 1;
                }else{
                    if(node.Operator.equals(BinaryOperation.BinaryOp.Times)){
                        node.Operator = BinaryOperation.BinaryOp.Lshift;
                        im.value = 1;
                    }
                }
            }else
            if(im.value == 64){
                //System.err.println("changed2");
                if(node.Operator.equals(BinaryOperation.BinaryOp.Divide)){
                    node.Operator = BinaryOperation.BinaryOp.Rshift;
                    im.value = 6;
                }else{
                    if(node.Operator.equals(BinaryOperation.BinaryOp.Times)){
                        node.Operator = BinaryOperation.BinaryOp.Lshift;
                        im.value = 6;
                    }
                }
            }if(im.value == 16){
                //System.err.println("changed2");
                if(node.Operator.equals(BinaryOperation.BinaryOp.Divide)){
                    node.Operator = BinaryOperation.BinaryOp.Rshift;
                    im.value = 4;
                }else{
                    if(node.Operator.equals(BinaryOperation.BinaryOp.Times)){
                        node.Operator = BinaryOperation.BinaryOp.Lshift;
                        im.value = 4;
                    }
                }
            }if(im.value == 4){
                //System.err.println("changed2");
                if(node.Operator.equals(BinaryOperation.BinaryOp.Divide)){
                    node.Operator = BinaryOperation.BinaryOp.Rshift;
                    im.value = 2;
                }else{
                    if(node.Operator.equals(BinaryOperation.BinaryOp.Times)){
                        node.Operator = BinaryOperation.BinaryOp.Lshift;
                        im.value = 2;
                    }
                }
            }if(im.value == 8){
                //System.err.println("changed2");
                if(node.Operator.equals(BinaryOperation.BinaryOp.Divide)){
                    node.Operator = BinaryOperation.BinaryOp.Rshift;
                    im.value = 3;
                }else{
                    if(node.Operator.equals(BinaryOperation.BinaryOp.Times)){
                        node.Operator = BinaryOperation.BinaryOp.Lshift;
                        im.value = 3;
                    }
                }
            }
        }
        String op = null;
        switch (node.Operator) {
            case Plus: op = "add"; break;
            case Minus: op = "sub"; break;
            case Times: op = "imul"; break;
            case Divide: op = "idiv"; break;
            case Module: op = "idiv"; break;
            case Lshift: op = "sal"; break;
            case Rshift: op = "sar"; break;
            case And: op = "and"; break;
            case Or: op = "or"; break;
            case Xor: op = "xor"; break;
            default: assert false;
        }
        if(op == "sal" || op == "sar"){
            out.print("mov rcx, ");
            node.rhs.accept(this);
            out.println();
            out.print("mov ");
            node.dest.accept(this);
            out.print(", ");
            node.lhs.accept(this);
            out.println();
            out.printf("%s ", op);
            node.dest.accept(this);
            out.println(", cl");
        }
        else
        if(op != "idiv") {
            if(node.dest instanceof PhysicalRegister && node.rhs instanceof PhysicalRegister &&
                    ((PhysicalRegister)node.dest).getName().equals(((PhysicalRegister)node.rhs).getName())){
               // out.println("push rax");
                out.print("mov rax, ");
                node.rhs.accept(this);
                out.println();
                out.print("mov ");
                node.dest.accept(this);
                out.print(", ");
                node.lhs.accept(this);
                out.println();
                out.printf("%s ", op);
                node.dest.accept(this);
                out.println(", rax");
                //out.println("pop rax");
            }
            else{if(node.dest instanceof PhysicalRegister && node.lhs instanceof PhysicalRegister
                    && ((PhysicalRegister)node.dest).getName().equals(((PhysicalRegister)node.lhs).getName()))
            {

            }else {
                out.print("mov ");
                node.dest.accept(this);
                out.print(", ");
                node.lhs.accept(this);
                out.println();
            }
            out.printf("%s ", op);
            node.dest.accept(this);
            out.print(", ");
            node.rhs.accept(this);
            out.println();}
        }else{
           // out.println("push rax");
           // out.println("push rdx");
           // out.println("push rcx");
            out.print("mov ");
            out.print("rax, ");
            node.lhs.accept(this);
            out.println();
            out.print("mov ");
            out.print("rcx, ");
            node.rhs.accept(this);
            out.println();
            out.println("xor rdx, rdx");
            out.println("cdq");
            out.println("idiv ecx");
            out.print("mov ");
            node.dest.accept(this);
            if(node.Operator.equals(BinaryOperation.BinaryOp.Divide)) out.println(", rax"); else out.println(", rdx");
           // out.println("pop rcx");
           // out.println("pop rdx");
           // out.println("pop rax");
        }
    }

    @Override
    public void visit(UnaryOperation node) {
        String op = null;
        switch (node.Operator) {
            case Not: op = "not"; break;
            case Oppo: op = "neg"; break;
            default: assert false;
        }
        out.printf("mov ");
        node.dest.accept(this);
        out.printf(", ");
        node.oper.accept(this);
        out.println();
        out.printf("%s ", op);
        node.dest.accept(this);
        out.println();

    }

    @Override
    public void visit(Comparison node) {
        String op = null;
        switch (node.Operator) {
            case Equal: op = "cmove"; break;
            case NotEqual: op = "cmovne"; break;
            case GreaterEqualThan: op = "cmovge"; break;
            case Greaterthan: op = "cmovg"; break;
            case LessThan: op = "cmovl"; break;
            case LessEqualThan: op = "cmovle"; break;
            default: assert false;
        }
       // out.println("push rax");
        out.println("xor rax, rax");
        out.print("cmp ");
        node.lhs.accept(this);
        out.print(", ");
        node.rhs.accept(this);
        out.println();
        out.printf("%s ", op);
        out.print("rax");
        out.println(", [_boolean_const_true]");
        out.print("mov ");
        node.dest.accept(this);
        out.println(", rax");
      //  out.println("pop rax");
    }

    @Override
    public void visit(Immediate node) {
        out.print(node.value);
    }

    @Override
    public void visit(Call node) {
        String now = blockLabel(node.function.beginBlock);
        if(node.function.name.equals("print"))now = "printwithoutenter";
        if(node.function.name.equals("println"))now = "printwithenter";
        if(node.function.name.equals("toString"))now = "tostring";
        if(node.function.name.equals("getString"))now = "getstr";
        if(node.function.name.equals("getInt"))now = "getint";
        if(node.function.name.equals("parseInt"))now = "parseint";
        if(node.function.name.equals("substring"))now = "substr";
        if(node.function.name.equals("length"))now = "strlen";
        if(node.function.name.equals("stringCompare"))now = "strcompare";
        if(node.function.name.equals("stringPlus"))now = "stringcat";
        if(node.function.name.equals("malloc"))now = "malloc";
        out.printf("call %s\n", now);
    }

    @Override
    public void visit(Syscall node) {
        out.println("    syscall");
    }

    @Override
    public void visit(CJump node) {
        out.print("cmp ");
        node.condition.accept(this);
        out.println(", 0");
        if(node.goelse !=null)out.printf("jz %s\n", blockLabel(node.goelse));
        if (blockNumber.get(currentBlock)+1 != blockNumber.get(node.gothen)) {
            out.printf("jmp %s\n", blockLabel(node.gothen));
        }
    }

    @Override
    public void visit(Return node) {
        out.println("ret");
    }

    @Override
    public void visit(Jump node) {
        if (blockNumber.get(currentBlock)+1 != blockNumber.get(node.target)) {
            out.println("jmp "+ blockLabel(node.target));
        }
    }

    @Override
    public void visit(VirtualRegister node) {
        assert false;
    }

    @Override
    public void visit(PhysicalRegister node) {
        out.print(node.getName());
    }

    @Override
    public void visit(StackSlot node) {
        out.print("[");
        node.address.accept(this);
        out.print(" - ");
        out.print(node.offset);
        out.print("]");
    }

    @Override
    public void visit(Allocate node) {
        out.println("    li $v0, 9");
        out.println("    syscall");
    }

    @Override
    public void visit(Load node) {
        out.print("mov ");
        node.dest.accept(this);
        if (node.isStaticData || node.address instanceof StackSlot) {
            out.print(", ");
            node.address.accept(this);
            out.println();
        } else {
            out.print(", [");
            node.address.accept(this);
            out.printf(" - %d", node.offset);
            out.println("]");
        }
    }

    @Override
    public void visit(Store node) {
        if(node.value instanceof Immediate){
        //    out.println("push rax");
            out.print("mov rax, ");
            node.value.accept(this);
            out.print("\nmov ");
            if (node.isStaticData || node.address instanceof StackSlot) {
                node.address.accept(this);
            } else {
                out.print("[");
                node.address.accept(this);
                out.printf(" - %d", node.offset);
                out.print("]");
            }
            out.print(", rax");
            out.println();
        //    out.println("pop rax");
        }else {
            out.print("mov ");
            if (node.isStaticData || node.address instanceof StackSlot) {
                node.address.accept(this);
            } else {
                out.print("[");
                node.address.accept(this);
                out.printf(" - %d", node.offset);
                out.print("]");
            }
            out.print(", ");
            node.value.accept(this);
            out.println();
        }
    }

    @Override
    public void visit(Move node) {
        if(!(node.rhs instanceof PhysicalRegister) && !(node.dest instanceof PhysicalRegister)){
          //  out.println("push rax");
            out.print("mov rax, ");
            node.rhs.accept(this);
            out.print("\nmov ");
            node.dest.accept(this);
            out.println(", rax\n");
          //  out.println("pop rax");
        }else{
        out.print("mov ");
        node.dest.accept(this);
        out.print(", ");
        node.rhs.accept(this);
        out.println();
    }
    }

    @Override
    public void visit(StaticSpace node) {
        if (isDefiningStaticData) out.printf("%s:\n times %d db 0\n", dataId(node), 8);
        else out.print("["+dataId(node)+"]");
    }

    @Override
    public void visit(StaticString node) {
        if (isDefiningStaticData) {
            out.printf("%s:\n", dataId(node));
            if(node.value.contains("\\\"")){

                out.print("db\t");
                out.print("\"");
                out.print(node.value.replace("\\\"","\",22,\"").replace("\\\\","\",94,\""));
                out.println("\"");
            }
            else if(node.value.equals("%seefg"))out.print("db\t\"%s\", 10, 0\n");
            else if(node.value.equals("buffer")) out.println("times 1000 db 0");
            else if(node.value.contains("\\n")) {
                out.print("db\t\"");
                out.print(node.value.substring(0, node.value.length()-2));
                out.println("\",10,0");
            }
            else out.printf("db\t\"%s\", 0\n",node.value);

        }
        else out.print(dataId(node));
    }
}

