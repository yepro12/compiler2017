package com.yepro12.compiler2017.Back;

import com.yepro12.compiler2017.AST.Binary;
import com.yepro12.compiler2017.IR.*;
import com.yepro12.compiler2017.Table.Type;

import java.util.*;
import static com.yepro12.compiler2017.Back.X86RegisterSet.*;

/**
 * Created by yepro12 on 2017/5/24.
 */
public class TargetIRPrinter {
    public int offsss = 0;
    public int ct = 0;
    public class FunctionInfo {
        int beginarg = 0;
        int beginra = 0;
        int beginebp = 0;
        int beginCalleesave = 0;
        int beginTemprary = 0;
        int totalSize = 0;
        List<PhysicalRegister> usedCallerSaveRegister = new ArrayList<>();
        List<PhysicalRegister> usedCalleeSaveRegister = new ArrayList<>();
        Map<StackSlot, Integer> stackSlotOffset = new HashMap<>();
        Set<PhysicalRegister> recursiveUsedRegister = new HashSet<>();
    }
    public IRRoot root;
    public Map<Function, FunctionInfo>infoMap = new HashMap<>();
    public void modifyStackSlot(Function func, FunctionInfo info, Block block, Instruction inst) {
        //System.err.println("beginSS");
        if (inst instanceof Load) {
            Load load = (Load) inst;
            if (load.address instanceof StackSlot) {
               // System.err.println("erw");
                if(info.stackSlotOffset.get(load.address)==null)return;
                ((StackSlot)load.address).offset = info.stackSlotOffset.get(load.address)+8;
                ((StackSlot)load.address).address = RBP;
            }
        } else if (inst instanceof Store) {
            Store store = (Store) inst;

            if (store.address instanceof StackSlot) {
               // System.err.println("erw");
                if(info.stackSlotOffset.get(store.address)==null)return;
                ((StackSlot)store.address).offset = info.stackSlotOffset.get(store.address)+8;
                ((StackSlot)store.address).address=RBP;
            }
        } else if (inst instanceof Push) {
            Push push = (Push) inst;
            if (push.now instanceof StackSlot) {
                ((StackSlot)push.now).offset = info.stackSlotOffset.get(push.now)+8;
                ((StackSlot)push.now).address=RBP;
            }
        } else if (inst instanceof Move) {
            Move move = (Move) inst;
            if (move.rhs instanceof StackSlot) {
                ((StackSlot)move.rhs).offset = info.stackSlotOffset.get(move.rhs)+8;
                ((StackSlot)move.rhs).address=RBP;
            }
        }
       // System.err.println("endSS");
    }
    public void modifyCall(Function func, FunctionInfo info, Block block, Instruction inst) {
       // System.err.println("beginCall");
        Call call = (Call) inst;

        Function callee = call.function;
        FunctionInfo calleeInfo = infoMap.get(callee);

          call.prepend(new Push(block, RBP));
          call.prepend(new Move(block, RBP, RSP));


          for (int i = 0; i < info.usedCallerSaveRegister.size(); ++i) {
            PhysicalRegister reg = info.usedCallerSaveRegister.get(i);
            inst.prepend(new Push(block, reg));

        }
        inst.prepend(new BinaryOperation(block, RSP, BinaryOperation.BinaryOp.Minus, RSP, new Immediate(8 *call.arguments.size())));
        for(int i = 0; i < call.arguments.size(); i++)
        {
            if(!(call.arguments.get(i) instanceof StackSlot))inst.prepend(new Move(block, RAX, call.arguments.get(i)));
            else inst.prepend(new Load(block, RAX, 8, RBP,
                    8-info.totalSize+info.stackSlotOffset.get(call.arguments.get(i))));
            inst.prepend(new Store(block, RAX, 8, RSP, -8*call.arguments.size()+i*8+8));
        }

        if(call.arguments.size() > 0)inst.prepend(new Load(block, RDI, 8, RSP, -8*call.arguments.size()+8));
        if(call.arguments.size() > 1)inst.prepend(new Load(block, RSI, 8, RSP, -8*call.arguments.size()+16));
        if(call.arguments.size() > 2)inst.prepend(new Load(block, RDX, 8, RSP, -8*call.arguments.size()+24));
        if(call.arguments.size() > 3)inst.prepend(new Load(block, RCX, 8, RSP, -8*call.arguments.size()+32));
        if(call.arguments.size() > 4)inst.prepend(new Load(block, R8, 8, RSP, -8*call.arguments.size()+40));
        if(call.arguments.size() > 5)inst.prepend(new Load(block, R9, 8, RSP, -8*call.arguments.size()+48));
        if(call.arguments.size()%2==1)
            inst.prepend(new BinaryOperation(block,RSP,BinaryOperation.BinaryOp.Minus,RSP,new Immediate(8)));

        if(call.function.type.returnType.type != Type.TheType.VOID)inst.append(new Move(block, call.dest, RAX));
        call.append(new Pop(block, RBP));
        call.append(new Move(block, RSP, RBP));
        for (int i = 0; i < info.usedCallerSaveRegister.size(); i++) {
            PhysicalRegister reg = info.usedCallerSaveRegister.get(i);
                inst.append(new Pop(block, reg));
        }
        inst.append(new BinaryOperation(block, RSP, BinaryOperation.BinaryOp.Plus, RSP, new Immediate(8 *call.arguments.size())));
        if(call.arguments.size()%2 == 1)
            inst.append(new BinaryOperation(block, RSP, BinaryOperation.BinaryOp.Plus, RSP, new Immediate(8)));


       // System.err.println("endCall");
    }
    public TargetIRPrinter(IRRoot root){
        this.root = root;
    }
    public void run(){
       // System.err.println("begin TargetIRPrinter");

        System.err.println("end CalcCallee");
        for(Function nowfunc: root.builtinFunctions){
            FunctionInfo tmpInfo = new FunctionInfo();
            tmpInfo.recursiveUsedRegister.addAll(nowfunc.usedPR);
            infoMap.put(nowfunc, tmpInfo);
        }
        for(Function nowfunc: root.functions.values()){
            System.err.printf("%s\n", nowfunc.name);
            FunctionInfo tmpInfo = new FunctionInfo();

            for(PhysicalRegister nowReg : X86RegisterSet.allRegisters)
                if(nowReg.isCallersave())tmpInfo.usedCallerSaveRegister.add(nowReg);
            else if(nowReg.isCalleesave())tmpInfo.usedCalleeSaveRegister.add(nowReg);
           // tmpInfo.usedCallerSaveRegister.add(RDI);
           // tmpInfo.usedCallerSaveRegister.add(RSI);
           // tmpInfo.usedCallerSaveRegister.add(RDX);
           // tmpInfo.usedCallerSaveRegister.add(RCX);
           // tmpInfo.usedCallerSaveRegister.add(R8);
           // tmpInfo.usedCallerSaveRegister.add(R9);
            tmpInfo.beginarg = 8 * tmpInfo.usedCallerSaveRegister.size();

            tmpInfo.beginra = tmpInfo.beginarg + nowfunc.paralist.size() * 8;
            if(nowfunc.paralist.size()%2 ==1)tmpInfo.beginra+=8;
            tmpInfo.beginCalleesave = tmpInfo.beginra + 8;
            System.err.printf("size=%d",tmpInfo.usedCalleeSaveRegister.size());
            System.err.printf("size=%d",X86RegisterSet.allRegisters.size());
            tmpInfo.beginTemprary = tmpInfo.beginCalleesave + tmpInfo.usedCalleeSaveRegister.size() * 8;
            for(int i = 0; i < nowfunc.stack.size(); i++)
                tmpInfo.stackSlotOffset.put(nowfunc.stack.get(i), tmpInfo.beginTemprary + i * 8);
            tmpInfo.beginebp = tmpInfo.beginTemprary + 8 * tmpInfo.stackSlotOffset.size();
            for(int i = 0; i < nowfunc.paralist.size(); i++)
                tmpInfo.stackSlotOffset.put(nowfunc.argStackSlotMap.get(nowfunc.paralist.get(i)), tmpInfo.beginarg + i * 8);
            if(tmpInfo.stackSlotOffset.size()%2==1)tmpInfo.beginebp += 8;
            tmpInfo.totalSize = tmpInfo.beginebp + 8;
            if(nowfunc.name.equals("main")){nowfunc.beginBlock.ofsss = tmpInfo.beginCalleesave; offsss = tmpInfo.beginCalleesave;}
            System.err.println(tmpInfo.beginarg);
            System.err.println(tmpInfo.beginra);
            System.err.println(tmpInfo.beginCalleesave);
            System.err.println(tmpInfo.beginTemprary);
            System.err.println(tmpInfo.beginebp);
            System.err.println(tmpInfo.totalSize);
            Set<Function> visit = new HashSet<>();
            visit.add(nowfunc);
            boolean isok = true;

            tmpInfo.recursiveUsedRegister.addAll(nowfunc.usedPR);
            while(isok){
                isok = false;
                Set<Function> tmpFunc = new HashSet<>();
                for(Function i : visit){
                    System.err.println(i.name);
                    for(Function j : i.callees)if(!visit.contains(j)){
                        isok = true;
                        tmpInfo.recursiveUsedRegister.addAll(j.usedPR);
                        tmpFunc.add(j);
                    }
                }
                visit.addAll(tmpFunc);
            }

            infoMap.put(nowfunc, tmpInfo);
        }
        System.err.println("end CalcFrame");
        for(Function nowfunc: root.functions.values()){
            System.err.println(nowfunc.type.name);
            FunctionInfo tmpinfo = infoMap.get(nowfunc);
            FunctionInfo info = infoMap.get(nowfunc);
            Block block = nowfunc.beginBlock;
            Instruction firstInst = block.begin;
            for (int i = 0; i < info.usedCalleeSaveRegister.size(); i++)
            if(firstInst != null){firstInst.prepend(new Push(block, info.usedCalleeSaveRegister.get(i)));}
            else block.finish(firstInst);

            firstInst.prepend(new BinaryOperation(block, RSP, BinaryOperation.BinaryOp.Minus, RSP, new Immediate(8 * tmpinfo.stackSlotOffset.size())));
            if(tmpinfo.stackSlotOffset.size()%2==1)
                firstInst.prepend(new BinaryOperation(block, RSP, BinaryOperation.BinaryOp.Minus, RSP, new Immediate(8)));
            System.err.println("mdi ret");
            if(nowfunc.type.returnType.type != Type.TheType.VOID)
                for(Return now : nowfunc.returns)now.prepend(new Move(now.currentBlock, RAX, now.ret));

            System.err.println("mdi ext");

            block = nowfunc.endBlock;
            Instruction lastInst = block.end;
            if(tmpinfo.stackSlotOffset.size()%2 == 1)
            lastInst.prepend(new BinaryOperation(block, RSP, BinaryOperation.BinaryOp.Plus, RSP, new Immediate(8 )));
            lastInst.prepend(new BinaryOperation(block, RSP, BinaryOperation.BinaryOp.Plus, RSP, new Immediate(8 * tmpinfo.stackSlotOffset.size())));

            for (int i = info.usedCalleeSaveRegister.size() - 1; i >= 0; i--)
                lastInst.prepend(new Pop(block, info.usedCalleeSaveRegister.get(i)));

            System.err.println("Edit");

            for (Block tblock : nowfunc.dfsOrder())
                for (Instruction inst = tblock.begin; inst != null; inst = inst.next)
                    if(inst instanceof Call)modifyCall(nowfunc, tmpinfo, tblock, inst);

            for (Block tblock : nowfunc.dfsOrder())
                for (Instruction inst = tblock.begin; inst != null; inst = inst.next)
                    if(inst instanceof Store || inst instanceof Load ||
                            inst instanceof Move || inst instanceof Push)modifyStackSlot(nowfunc, tmpinfo, tblock, inst);

        }
        System.err.println("end ModifyInstructions");
    }

}
