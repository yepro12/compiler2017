package com.yepro12.compiler2017.Back;
import com.yepro12.compiler2017.Table.GlobalTable;
import com.yepro12.compiler2017.IR.*;
import java.util.*;

/**
 * Created by yepro12 on 2017/5/31.
 */
public class GlobalVariableTransformer {
    public IRRoot root;
    public static class FunctionInfo {
        Map<StaticData, VirtualRegister> staticMap = new HashMap<>();
        Set<StaticData> writtenStatic = new HashSet<>();
        Set<StaticData> recursiveStaticUse = new HashSet<>();
    }
    public Map<Function, FunctionInfo> funcInfo = new HashMap<>();

    public GlobalVariableTransformer(IRRoot root) {
        this.root = root;
    }

    public VirtualRegister getVR(StaticData data, Map<StaticData, VirtualRegister> staticMap) {
        VirtualRegister reg = staticMap.get(data);
        if (reg == null) {
            reg = new VirtualRegister(data.name);
            staticMap.put(data, reg);
        }
        return reg;
    }

    public void processFunction(Function func) {
        FunctionInfo info = new FunctionInfo();
        funcInfo.put(func, info);
        Map<StaticData, VirtualRegister> staticMap = info.staticMap;
        Set<StaticData> writtenStatic = info.writtenStatic;
        Map<Register, Register> renameMap = new HashMap<>();

        for (Block block: func.dfsOrder())
            for (Instruction inst = block.begin; inst != null; inst = inst.next) {
                if ((inst instanceof Load && ((Load) inst).isStaticData) ||
                        (inst instanceof Store && ((Store) inst).isStaticData)) continue;
                Collection<Register> used = inst.getUsedRegisters();
                if (!used.isEmpty()) {
                    renameMap.clear();
                    used.forEach(x -> renameMap.put(x, x));
                    for (Register register : used)
                        if (register instanceof StaticData) {
                        System.err.println(((StaticData) register).name);
                            StaticData data = (StaticData) register;
                            renameMap.put(register, getVR(data, staticMap));
                        }
                    inst.setUsedRegister(renameMap);
                }
                for(Register defined:inst.getDefinedRegisters())
                if (defined instanceof StaticData) {
                    System.err.println(((StaticData) defined).name);
                    VirtualRegister reg = getVR((StaticData) defined, staticMap);
                    inst.setDefinedRegister(reg);
                    writtenStatic.add((StaticData) defined);
                }
            }

        Block block = func.beginBlock;
        Instruction inst = block.begin;
        if(inst instanceof Move)System.err.println("feee");
        if(inst.prev == null)System.err.println("rigth");
        staticMap.forEach((data, vr) -> inst.prepend(new Load(block, vr, 8, data, data instanceof StaticString)));
        if(inst.prev == null)System.err.println("wrong");
        staticMap.forEach((data, vr) -> System.err.println(data.name));
        System.err.printf("efer=%d\n",staticMap.size());
    }

    public void calcCallees() {
        root.builtinFunctions.forEach(x -> funcInfo.put(x, new FunctionInfo()));
        for(Function nowfunc: root.functions.values()) {
            nowfunc.callees.add(nowfunc);
            for (Block block : nowfunc.dfsOrder())
                for (Instruction inst = block.begin; inst != null; inst = inst.next)
                    if (inst instanceof Call) nowfunc.callees.add(((Call) inst).function);
        }
        boolean ok = false;
        while (!ok){
            ok = true;
            for(Function nowfunc: root.functions.values())
            {
                Set<Function> appen = new HashSet<>();
                for(Function callee: nowfunc.callees)
                    for(Function recurcallee: callee.callees)
                        if(!nowfunc.callees.contains(recurcallee)){
                            ok = false;
                            appen.add(recurcallee);
                        }
                nowfunc.callees.addAll(appen);
            }
        }
        System.err.println("haa");
        for(Function func : root.functions.values()){
            System.err.print(func.name);System.err.println(":");
            for(Function ca: func.callees)System.err.println(ca.name);
        }
        for (Function func : root.functions.values()) {
            FunctionInfo info = funcInfo.get(func);
            info.recursiveStaticUse.addAll(info.staticMap.keySet());
            func.callees.forEach(x -> info.recursiveStaticUse.addAll(funcInfo.get(x).staticMap.keySet()));
           // func.callees.forEach(x-> funcInfo.get(func).writtenStatic.addAll(funcInfo.get(x).writtenStatic));
        }
    }

    public void run() {
        for (Function func : root.functions.values()) processFunction(func);

        calcCallees();

        Set<StaticData> reloadSet = new HashSet<>();
        for (Function func : root.functions.values()) {
            FunctionInfo info = funcInfo.get(func);
            Set<StaticData> usedSet = info.staticMap.keySet();
            if (usedSet.isEmpty()) continue;
            for (Block block : func.dfsOrder()) {
                for (Instruction inst = block.begin; inst != null; inst = inst.next) {
                    if (!(inst instanceof Call)) continue;
                    Call call = (Call) inst;
                    FunctionInfo calleeInfo = funcInfo.get(call.function);
                    for (StaticData data : info.writtenStatic)
                        if (calleeInfo.recursiveStaticUse.contains(data))
                            call.prepend(new Store(block, info.staticMap.get(data),8, data));
                  //  if (calleeInfo == null) continue;
                  //  if (calleeInfo.writtenStatic.isEmpty()) continue;
                    reloadSet.clear();
                    Set<StaticData> tmp = new HashSet<>();
                    tmp.addAll(calleeInfo.writtenStatic);
                    call.function.callees.forEach(x -> tmp.addAll(funcInfo.get(x).writtenStatic));
                    reloadSet.addAll(tmp);
                    reloadSet.retainAll(usedSet);
                 //   System.err.print("compare:");System.err.print(func.name);
                  //  System.err.print(",");System.err.println(call.function.name);
                   // calleeInfo.writtenStatic.forEach(x -> System.err.println(x.name));
                    //System.err.println("-----");
                    //usedSet.forEach(x->System.err.println(x.name));
                    //System.err.println();
                    for (StaticData data : reloadSet) {
                        call.append(new Load(block, info.staticMap.get(data), 8, data, data instanceof StaticString));
                      //  System.err.println("reload");
                       // System.err.println(call.function.name);
                        //System.err.println(func.name);
                    }
                }
            }
        }

        for (Function func : root.functions.values()) {
            Return ret = func.returns.get(0);
            FunctionInfo info = funcInfo.get(func);
            for (StaticData data : info.writtenStatic)
                ret.prepend(new Store(ret.currentBlock, info.staticMap.get(data), 8, data));
        }
    }
}
