package com.yepro12.compiler2017.Back;

import com.yepro12.compiler2017.IR.*;

import java.sql.SQLType;
import java.util.*;
/**
 * Created by yepro12 on 2017/6/9.
 */
public class LivelinessAnalyzer {
    public IRRoot root;
    public LivelinessAnalyzer(IRRoot root){
        this.root = root;
    }
    public void processFunc(Function nowfunc){
        //System.err.println("funcname:");
        //System.err.println(nowfunc.name);
        List<Block> blocks = nowfunc.dfsOrder();
        Set<VirtualRegister> in = new HashSet<>();
        Set<VirtualRegister> out = new HashSet<>();
        for(Block block: blocks){
            for(Instruction inst = block.begin; inst != null; inst = inst.next){
                inst.liveIn.clear();
                inst.liveOut.clear();
            }
        }
        boolean ok = false;
        while(!ok){
            ok = true;
            for(Block block : blocks){
                for(Instruction inst = block.end; inst!= null; inst = inst.prev){
                    in.clear();out.clear();
                   // System.err.println("begin inst");
                    if(inst instanceof CJump){
                      //  System.err.println("into cj");
                        CJump cj = (CJump)inst;
                        out.addAll(cj.goelse.begin.liveIn);
                        out.addAll(cj.gothen.begin.liveIn);
                    } else
                    if(inst instanceof Jump){
                       // System.err.println("into jmp");
                        Jump jmp = (Jump)inst;
                        //System.err.println(jmp.target.name);
                        if(jmp.target.begin!=null)out.addAll(jmp.target.begin.liveIn);
                    } else
                    if(!(inst instanceof Return)){
                        //System.err.println("into other");
                       // System.err.println("gotc");
                        assert inst.next != null;
                       // System.err.println("gota");
                        out.addAll(inst.next.liveIn);
                    }
                    //System.err.println("begin add");
                    in.addAll(out);
                    inst.getDefinedRegisters().stream().filter(x->x instanceof VirtualRegister).forEach(x->in.remove(x));
                    inst.getUsedRegisters().stream().filter(x->x instanceof VirtualRegister).forEach(x->in.add((VirtualRegister) x));
                   // System.err.println("begin elim");
                    if(!inst.liveIn.equals(in)||!inst.liveOut.equals(out)){
                        ok = false;
                        inst.liveIn.clear();
                        inst.liveIn.addAll(in);
                        inst.liveOut.clear();
                        inst.liveOut.addAll(out);
                    }
                    //System.err.println("finish inst");
                }
            }
        }
    }
    public void run(){
        System.err.println("begin Analysis");
        root.functions.values().forEach(this::processFunc);
        System.err.println("end Anaylysis");
    }
}
