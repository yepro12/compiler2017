package com.yepro12.compiler2017.Back;

import com.yepro12.compiler2017.IR.PhysicalRegister;

import java.util.*;

/**
 * Created by yepro12 on 2017/5/24.
 */
public class X86RegisterSet {
    public static X86Register RAX = new X86Register(0, "rax", false,false);
    public static X86Register RBX = new X86Register(1, "rbx", false,false);
    public static X86Register RCX = new X86Register(2, "rcx", false,false);
    public static X86Register RDX = new X86Register(3, "rdx", false,false);
    public static X86Register RSI = new X86Register(4, "rsi", false,false);
    public static X86Register RDI = new X86Register(5, "rdi", false,false);
    public static X86Register RSP = new X86Register(6, "rsp", false,false);
    public static X86Register RBP = new X86Register(7, "rbp", false,false);
    public static X86Register R8 = new X86Register(8, "r8", false,false);
    public static X86Register R9 = new X86Register(9, "r9", false,false);
    public static X86Register R10 = new X86Register(10, "r10", false,false);
    public static X86Register R11 = new X86Register(11, "r11", false,false);
    public static X86Register R12 = new X86Register(12, "r12", false,true);
    public static X86Register R13 = new X86Register(13, "r13", false,true);
    public static X86Register R14 = new X86Register(14, "r14", false,true);
    public static X86Register R15 = new X86Register(15, "r15", false,true);
    public static Collection<PhysicalRegister> allRegisters;
    static {
        allRegisters = new ArrayList<>();
        allRegisters.add(R12);
        allRegisters.add(R13);
        allRegisters.add(R14);
        allRegisters.add(R15);
    }
}
