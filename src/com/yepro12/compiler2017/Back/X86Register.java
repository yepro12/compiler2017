package com.yepro12.compiler2017.Back;

import com.yepro12.compiler2017.IR.PhysicalRegister;

/**
 * Created by yepro12 on 2017/5/24.
 */
public class X86Register extends PhysicalRegister {
    public int id;
    public String name;
    public boolean callersave;
    public boolean calleesave;
    public X86Register(int id, String name, boolean callersave, boolean calleesave){
        this.id = id;
        this.name = name;
        this.callersave = callersave;
        this.calleesave = calleesave;
    }
    @Override
    public String getName(){
        return name;
    }
    @Override
    public boolean isCallersave(){
        return callersave;
    }
    @Override
    public boolean isCalleesave() { return calleesave; }
}
