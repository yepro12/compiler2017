package com.yepro12.compiler2017.Back;

import com.yepro12.compiler2017.IR.*;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yepro12 on 2017/5/27.
 */
public class IRPrinter implements IRvisitor {
    public PrintStream out;
    public Map<String, Integer> accumulater = new HashMap<>();
    public Map<Object, String> nameMap = new HashMap<>();
    public Map<Block, Integer> blockNumber = new HashMap<>();
    public Block currentBlock;
    public boolean isDefiningStaticData;

    public IRPrinter(PrintStream out) {
        this.out = out;
    }

    public String newId(String name) {
        int cnt = accumulater.getOrDefault(name, 0) + 1;
        accumulater.put(name, cnt);
        return name + "_" + cnt;
    }

    public String blockLabel(Block block) {
        if(block == null)return "null";
        String id = nameMap.get(block);
        if (id == null) {
            id = newId(block.name);
            nameMap.put(block, id);
        }
        return id;
    }

    public String dataId(StaticData node) {
        String id = nameMap.get(node);
        if (id == null) {
            id = newId(node.name);
            nameMap.put(node, id);
        }
        return id;
    }
    @Override
    public void visit(Push node){
        out.printf("Push ");
        node.now.accept(this);
        out.println();
    }
    @Override
    public void visit(Pop node){
        out.printf("Pop ");
        node.now.accept(this);
        out.println();
    }
    @Override
    public void visit(IRRoot node) {
        node.stringPool.values().forEach(this::visit);
        node.dataList.forEach(x -> x.accept(this));
        node.functions.values().stream().filter(x -> !(node.builtinFunctions.contains(x))).forEach(this::visit);
    }

    @Override
    public void visit(Block node) {
        out.printf("%s:\n", blockLabel(node));
        currentBlock = node;
        for (Instruction i = node.begin; i != null; i = i.next) i.accept(this);
    }

    @Override
    public void visit(Function node) {
        System.err.println(node.name);
      //  List<Block> tmp = node.dfsOrder();
      //  System.err.println(tmp.size());
      //  for (int i = 0; i < tmp.size(); ++i) blockNumber.put(tmp.get(i), i);
       // tmp.forEach(this::visit);
        for(Block now: node.dfsOrder())now.accept(this);
    }

    @Override
    public void visit(BinaryOperation node) {
        String op = null;
        switch (node.Operator) {
            case Plus: op = "+"; break;
            case Minus: op = "-"; break;
            case Times: op = "*"; break;
            case Divide: op = "/"; break;
            case Module: op = "%"; break;
            case Lshift: op = "<<"; break;
            case Rshift: op = ">>"; break;
            case And: op = "&"; break;
            case Or: op = "|"; break;
            case Xor: op = "^"; break;
            default: assert false;
        }
        node.dest.accept(this);
        out.print(" = ");
        node.lhs.accept(this);
        out.printf("%s ",op);
        node.rhs.accept(this);
        out.println();
    }

    @Override
    public void visit(UnaryOperation node) {
        String op = null;
        switch (node.Operator) {
            case Not: op = "~"; break;
            case Oppo: op = "-"; break;
            default: assert false;
        }
        node.dest.accept(this);
        out.printf("%s ", op);
        node.oper.accept(this);
        out.println();
    }

    @Override
    public void visit(Comparison node) {
        String op = null;
        switch (node.Operator) {
            case Equal: op = "=="; break;
            case NotEqual: op = "!="; break;
            case GreaterEqualThan: op = ">="; break;
            case Greaterthan: op = ">"; break;
            case LessThan: op = "<"; break;
            case LessEqualThan: op = "<="; break;
            default: assert false;
        }
        node.dest.accept(this);
        out.print(" = ");
        node.lhs.accept(this);
        out.print(op);
        node.rhs.accept(this);
        out.println();
    }

    @Override
    public void visit(Immediate node) {
        out.print(node.value);
    }

    @Override
    public void visit(Call node) {
        if(node.dest != null){
        node.dest.accept(this);
        out.print(" = ");}
        out.printf("%s(", node.function.name);
        for(int i = 0; i < node.arguments.size(); i++){
            node.arguments.get(i).accept(this);
            out.printf(",");
        }
        out.println(")");
    }

    @Override
    public void visit(Syscall node) {
        out.println("    syscall");
    }

    @Override
    public void visit(CJump node) {
        out.print("IF(");
        node.condition.accept(this);
        out.println(")");
        out.printf("%s", blockLabel(node.gothen));
        out.println("ELSE");
        out.printf("%s\n", blockLabel(node.goelse));
    }

    @Override
    public void visit(Return node) {
        out.print("return ");
        if(node.ret != null) {
            node.ret.accept(this);
            out.println();
        }else out.println();
    }

    @Override
    public void visit(Jump node) {
        out.printf("goto %s\n", blockLabel(node.target));
    }

    @Override
    public void visit(VirtualRegister node) {
        if(node.name == null)out.print("Vr");else out.printf("%s",node.name);
    }

    @Override
    public void visit(PhysicalRegister node) {
        out.print(node.getName());
    }

    @Override
    public void visit(StackSlot node) {
        out.print(node.name);
    }

    @Override
    public void visit(Allocate node) {
        out.print("Allocate");
        node.dest.accept(this);
        out.print(" ");
        node.dest.accept(this);
        out.println("");
    }

    @Override
    public void visit(Load node) {
        out.print("load ");
        node.dest.accept(this);
        if(node.isStaticData){
            out.print("&");
            node.address.accept(this);
        }else{
            node.address.accept(this);
            out.print("+");
            out.printf("%d", node.offset);
        }
        out.println();
    }

    @Override
    public void visit(Store node) {
        out.print("store ");
        if(node.isStaticData){
            out.print("&");
            node.address.accept(this);
        }else{
            node.address.accept(this);
            out.print("+");
            out.printf("%d", node.offset);
        }
        out.print(", ");
        node.value.accept(this);
        out.println();
    }

    @Override
    public void visit(Move node) {
        out.print("mov ");
        node.dest.accept(this);
        out.print(", ");
        if(node.rhs == null)out.println("13n");
        node.rhs.accept(this);
        out.println();
    }

    @Override
    public void visit(StaticSpace node) {
        if (isDefiningStaticData) out.printf("%s: .space %d\n", dataId(node), 8);
        else out.print(dataId(node));
    }

    @Override
    public void visit(StaticString node) {
        if (isDefiningStaticData) {
            out.printf("%s:\n", dataId(node));
            out.print("db    ");
            out.printf("\"%s\", 0\n",node.value);
        }
        else out.print(dataId(node));
    }
}

