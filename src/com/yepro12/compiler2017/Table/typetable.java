package com.yepro12.compiler2017.Table;
import java.util.LinkedHashMap;
import java.util.Map;

public class typetable{
    public Map<String, Info> map = new LinkedHashMap<>();
    public typetable heritage;
    public boolean isGlobal = false;
    public int offset = 0;
    public typetable(typetable heritage){
        this.heritage = heritage;
    }
    public typetable(){
        this.heritage = null;
        this.map = new LinkedHashMap<>();
        this.isGlobal = true;
    }
    public void declare(String name, Type type){
        map.put(name, new Info(type, offset));
        offset += 8;
    }
    public Info getInfo(String name){
        Info now = map.get(name);
        if(now != null)return  now;
        if(heritage != null)return heritage.getInfo(name);
        return null;
    }
    public Type getType(String name){
        Info now = getInfo(name);
        return now == null ? null : now.type;
    }
    public Info getInfoCurrent(String name){
        return map.get(name);
    }
    public Type getTypeCurrent(String name){
        Info now = getInfoCurrent(name);
        return now == null ? null : now.type;
    }
}
