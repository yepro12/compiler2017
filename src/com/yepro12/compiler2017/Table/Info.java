package com.yepro12.compiler2017.Table;

import com.yepro12.compiler2017.IR.Register;

/**
 * Created by yepro12 on 2017/5/21.
 */
public class Info {
    public Type type;
    public int offset;
    public Register register = null;
    public Info(Type type, int offset){
        this.type = type;
        this.offset = offset;
    }
}
