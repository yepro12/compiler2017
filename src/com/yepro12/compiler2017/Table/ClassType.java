package com.yepro12.compiler2017.Table;
public class ClassType extends VariableType{
    public String name;
    public typetable member;
    public String constructor = null;
    public ClassType(String name){
        this.type = TheType.CLASS;
        this.name = name;
        this.member = new typetable(null);
    }

    @Override
    public boolean isSame(Type arg){
        if(arg.type==TheType.NULL)return true;
        if(arg instanceof ClassType){
            ClassType tmp = (ClassType)arg;
            return tmp.name == name;
        }else return  false;
    }
}